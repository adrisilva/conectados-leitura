<?php

Route::group(['domain' => '{school}.'.env('MAIN_DOMAIN')], function(){

    Route::get('first_access', 'UserController@firstAccess')->name('user.first_access');
    Route::get('first_access_admin', 'UserController@firstAccessAdmin')->name('user.first_access');

    Route::put('change_password', 'UserController@changePassword')->name('user.change_password');
    Route::put('change_password_admin', 'UserController@changePasswordAdmin')->name('admin.change_password');

    Route::group(['middleware' => 'verify.first_access'], function(){
        Route::get('/', 'SiteController@index')->name('site.index');
        Route::get('/sobre', 'SiteController@sobre')->name('site.sobre');
        Route::get('/contato', 'SiteController@contato')->name('site.contato');

        Route::get('/livros/{categoria}', 'SiteLivroController@list')->name('site.livros.list');
        Route::any('/livros-search', 'SiteLivroController@search')->name('site.livros.search');
        Route::get('/livro/show/{id}', 'SiteLivroController@show')->name('site.livro.show');

        Route::group(['middleware' => ['auth']], function(){
            Route::post('/reservar/livro', 'SiteLivroController@bookReserve')->name('site.livro.reserve');
            Route::post('/recomendar/livro', 'RecomendacaoController@recommendBook')->name('recomendar.livro');
            Route::post('/changePassword', 'UserController@changePassword')->name('changePassword');
            Route::get('/historico', 'UserController@showHistoric')->name('historic');
            Route::get('/recomendacoes', 'UserController@showRecomendations')->name('recomendations');
            Route::delete('/cancelar/reserva', 'UserController@reserveCancel')->name('site.user.cancel.reserve');
            Route::post('/rating-book', 'UserController@bookRating')->name('book.rating');

            Route::get('remover/{id}/recomendacao', 'RecomendacaoController@cancel');
        });

        Route::post('login', 'Auth\LoginController@login')->name('login');
        Route::post('logout', 'Auth\LoginController@logout')->name('logout');

        Route::group(['namespace' => 'Admin', 'middleware' => 'verify.first_access'], function(){

            Route::get('admin', 'Auth\LoginController@showLoginForm')->name('admin.login');

            Route::post('admin', 'Auth\LoginController@login')->name('admin.login.send');
            Route::post('admin-logout', 'Auth\LoginController@logout');

            Route::prefix('admin')->group(function(){

                Route::get('dashboard', 'AdminController@index')->name('admin.dashboard');

                Route::prefix('cadastro')->group(function(){
                    Route::get('aluno', 'AlunoController@exibirFormularioCadastro')->name('aluno.cadastro');
                    Route::post('aluno', 'AlunoController@store');
                    Route::get('professor', 'ProfessorController@exibirFormularioCadastro')->name('professor.cadastro');
                    Route::post('professor', 'ProfessorController@store');

                    Route::get('livro', 'LivroController@exibirFormularioCadastro')->name('livro.cadastro');
                    Route::post('livro', 'LivroController@store');
                });

                Route::prefix('editar')->group(function(){
                    Route::get('aluno/{id}', 'AlunoController@edit')->name('aluno.edit');
                    Route::put('aluno/{id}', 'AlunoController@update')->name('aluno.update');
                    Route::get('professor/{id}', 'ProfessorController@edit')->name('professor.edit');
                    Route::put('professor/{id}', 'ProfessorController@update')->name('professor.update');

                    Route::get('livro/{id}', 'LivroController@edit')->name('livro.edit');
                    Route::put('livro/{id}', 'LivroController@update')->name('livro.update');
                });

                Route::prefix('delete')->group(function(){
                    Route::delete('aluno', 'AlunoController@delete')->name('aluno.delete');
                    Route::delete('professor', 'ProfessorController@delete')->name('professor.delete');
                    Route::delete('livro', 'LivroController@delete')->name('livro.delete');
                });

                Route::get('alunos', 'AlunoController@list')->name('aluno.listar');
                Route::get('professores/', 'ProfessorController@list')->name('professor.listar');

                Route::get('alunos/buscar', 'AlunoController@search')->name('aluno.search');
                Route::get('professores/buscar', 'ProfessorController@search')->name('professor.search');

                Route::get('livros', 'LivroController@list')->name('livro.list');
                Route::any('livros-search', 'LivroController@search')->name('livro.search');


                Route::get('alugueis', 'RentController@list')->name('verificar.alugueis');
                Route::any('alugueis/buscar', 'RentController@search')->name('aluguel.search');

                Route::get('reservas', 'ReserveController@list')->name('verificar.reservas');
                Route::any('reserva/buscar', 'ReserveController@search')->name('reserva.search');


                Route::get('{origin}/ver/detalhes/usuario/{id}', 'AdminController@showUser')->name('user.show');
                Route::get('alugar/buscar/aluno', 'AlunoController@searchLite')->name('aluno.search-lite');
                Route::get('professor/buscar/professor', 'ProfessorController@searchLite')->name('professor.search-lite');

                Route::get('alugar/livro/{id}', 'RentController@exibirFormularioAluguel')->name('livro.aluguel');
                Route::post('rent', 'RentController@rent')->name('rent');
                Route::get('renovar/aluguel/{id}', 'RentController@renewBook')->name('renewBook');
                Route::post('returnBook', 'RentController@returnBook')->name('returnBook');
                Route::post('renovar/aluguel', 'RentController@completeRenovation')->name('completeRenovation');

                Route::get('reservas/realizar/aluguel/{id}', 'ReserveController@showRentForm')->name('reservado.alugar');
                Route::post('reserva/concluir/reserva', 'ReserveController@saveRent')->name('saveRent');
                Route::delete('cancelar/reserva', 'ReserveController@reserveCancel')->name('cancelReserve');
            });
        });
    });

});