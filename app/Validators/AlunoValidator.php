<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class AlunoValidator.
 *
 * @package namespace App\Validators;
 */
class AlunoValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name'          => 'required|string',
            'birth'         => 'required',
            'registration'  => 'required|unique:users,registration|min:7|max:7',
            'email'         => 'required|unique:users,email',
            'course'        => 'required', 
            'shift'         => 'required',
            'series'        => 'required',
        ],

        ValidatorInterface::RULE_UPDATE => [
            'name'          => 'required|string',
            'birth'         => 'required',
            'registration'  => 'required|unique:users,registration|min:7|max:7',
            'email'         => 'required|unique:users,email',
            'password'      => 'nullable|min:6',
            'course'        => 'required', 
            'shift'         => 'required',
            'series'        => 'required',
        ],

    ];

    protected $messages = [
        'name.required'         => 'Nome Obrigatório',
        'name.string'           => 'Somente letras no nome',
        'birth.required'        => 'Data de Nascimento Obritória',
        'registration.required' => 'Matrícula Obrigarória',
        'registration.min'      => 'Matrícula Inválida(minímo 7 caracteres)',
        'registration.max'      => 'Matrícula Inválida(máximo 7 caracteres)',
        'email.required'        => 'E-Mail Obrigatório',
        'password.required'     => 'Senha Obrigarória',
        'password.min'          => 'Senha de no minímo 6 caracteres',
        'email.unique'          => 'Esse e-mail já está em uso',
        'course.required'       => 'Curso Obrigatório',
        'series.required'       => 'Série Obrigatória',
        'shift.required'        => 'Turno Obrigatório',
        'registration.unique'   => 'Essa matrícula já está em uso'
    ];
}
