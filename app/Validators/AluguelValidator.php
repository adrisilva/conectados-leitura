<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class AluguelValidator.
 *
 * @package namespace App\Validators;
 */
class AluguelValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
        	'book_id' 		   => 'required',
        	'user_id' 		   => 'required',
        	'reservation_date' => 'required',
            //'user_id'          => 'unique:reserves,user_id,status,alugado',
        ],
        ValidatorInterface::RULE_UPDATE => [],
    ];

    protected $messages = [
        'user_id.unique'    => 'Esse usuário já possui um livro alugado!',
    ];
}
