<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class UserValidator.
 *
 * @package namespace App\Validators;
 */
class UserValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
        	'new_password' 		=> 'required|min:6|max:20',
        	'confirm_password'	=> 'required|min:6|max:20|same:new_password',
        ],
        ValidatorInterface::RULE_UPDATE => [],
    ];
}
