<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class LivroValidator.
 *
 * @package namespace App\Validators;
 */
class LivroValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
        	'title' 	  => 'required',
        	'category'	  => 'required',
        	'amount'	  => 'required|min:1',
        	'author'	  => 'required',
        	'cover'		  => 'required|image|mimes:jpeg, png, jpg',
            'description' => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'title'       => 'required',
            'category'    => 'required',
            'amount'      => 'required|min:1',
            'author'      => 'required',
            'cover'       => 'image|mimes:jpeg, png, jpg',
            'description' => 'required',
        ],
    ];

    protected $messages = [
        'title.required'        => 'Título do livro obrigatório',
        'category.required'     => 'Categoria do livro obrigatória',
        'amount.required'       => 'Quantidade de livros obrigatória',
        'amount.min'            => 'Quantidade não pode ser menor que 0(zero)',
        'author.required'       => 'Nome do autor do livro obrigatório',
        'cover.required'        => 'Capa do livro obrigatória',
        'cover.image'           => 'Arquivo inválido! Envie apenas imagens',
        'cover.mimes'           => 'Envie apenas arquivos do tipo: .jpg, .png, .jpeg',
        'description.required'  => 'Descrição obrigatória',
    ];
}
