<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class RecomendacaoValidator.
 *
 * @package namespace App\Validators;
 */
class RecomendacaoValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
        	'book_id' => 'unique:recommendations,book_id',
        ],
        ValidatorInterface::RULE_UPDATE => [],
    ];

    protected $messages = [
        'book_id.unique' => "Esse Livro já foi recomendando",
    ];
}
