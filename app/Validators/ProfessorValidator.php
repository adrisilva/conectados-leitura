<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class ProfessorValidator.
 *
 * @package namespace App\Validators;
 */
class ProfessorValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
        	'name'          => 'required|string',
            'birth'         => 'required',
            'registration'  => 'required|unique:users,registration|min:7|max:7',
            'email'         => 'required|unique:users,email',
            'matter'        => 'required', 
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name'          => 'required|string',
            'birth'         => 'required',
            'registration'  => 'required|unique:users,registration|min:7|max:7',
            'email'         => 'required|unique:users,email',
            'password'      => 'nullable|min:6',
            'matter'		=> 'required',
        ],
    ];

    protected $messages = [
        'name.required'         => 'Nome Obrigatório',
        'name.string'           => 'Somente letras no nome',
        'birth.required'        => 'Data de Nascimento Obritória',
        'registration.required' => 'Matrícula Obrigarória',
        'registration.min'      => 'Matrícula Inválida(minímo 7 caracteres)',
        'registration.max'      => 'Matrícula Inválida(máximo 7 caracteres)',
        'email.required'        => 'E-Mail Obrigatório',
        'password.required'     => 'Senha Obrigarória',
        'password.min'          => 'Senha de no minímo 6 caracteres',
        'email.unique'          => 'Esse e-mail já está em uso',
        'registration.unique'   => 'Essa matrícula já está em uso',
        'matter.required'       => 'Matéria Obrigarória'
    ];
}
