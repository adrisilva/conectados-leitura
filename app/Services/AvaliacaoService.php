<?php 

namespace App\Services;

use App\Entities\Rating;
use App\Repositories\RatingRepository;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use Exception;

class AvaliacaoService
{

	protected $repository;

	public function __construct(RatingRepository $repository)
	{
		$this->repository = $repository;
	}

	public function bookRating($data)
	{
		$rating = $this->repository->scopeQuery(function($query) use($data){
			return $query->where('user_id', $data['user_id'])
						->where('book_id', $data['book_id']);
		})->all();

		if(count($rating) == 1){
			$this->repository->update($data, $rating[0]['id']);
			return [
				'success' 		=> true,
				'messages'	 	=> 'Obrigado por sua avaliação',
				'messages_info' => 'A Sua avaliação foi ataulizada com sucesso.',
			];
		}

		$this->repository->create($data);
		return [
			'success' 		=> true,
			'messages'	 	=> 'Obrigado por sua avaliação',
			'messages_info' => 'Sua a avaliação foi enviada com sucesso! Sinta-se a vontade para mudar-la, se assim desejar.',
		];
	}

}