<?php 

namespace App\Services;

use App\Repositories\UserRepository;
use App\Validators\ProfessorValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use Exception;

class ProfessorService
{

	private $repository;
    private $validator;
    private $perPage;

    public function __construct(UserRepository $repository, ProfessorValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->perPage    = 15;
    } 

    public function store(array $data)
    {
    	try{
            $password = generatePassword();

            $data['password'] = bcrypt($password);
            $data['type']	  = "Professor";

            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE); 

            $professor = $this->repository->create($data);
            if($professor){
                $data['password'] = $password;
                sendEmailPassword($data);
            }

            return [
                'success' => true,
                'messages' => "Cadastro realizado com sucesso.",
                'messages_info' => "A senha de acesso da conta do professor foi enviada para o e-mail do mesmo.",
            ];

        }catch(Exception $ex){

            switch(get_class($ex))
            {
                //case QueryException::class      : return ['success' => false  ,'messages' => $ex->getMessage()];
                case ValidatorException::class  : return ['success' => false  ,'messages' => $ex->getMessageBag(), 'messages_info' => null];
                //case Exception::class           : return ['success' => false  ,'messages' => $ex->getMessage()];
                //default                         : return ['success' => false  ,'messages' => $ex->getMessage(), 'messages_info' => null];
            }

        }
    }

    public function list()
    {
        $professores = $this->repository->scopeQuery(function($query){
            return $query->where('type', 'Professor')
                        ->orderBy('name', 'asc');                          
        });

        return $professores->paginate($this->perPage);
    }

    public function update(array $data, $id)
    {
        try{

            $this->validator->setId($id);
            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE); 
            
            if($data['password'] != null)
                $data['password'] = bcrypt($data['password']);
            else
                unset($data['password']);

            $professor = $this->repository->update($data, $id);

            return [
                'success' => true,
                'messages' => "Dados atualizados com sucesso.",
                'messages_info' => null,
            ];

        }catch(Exception $ex){
            switch(get_class($ex)){
                case ValidatorException::class  : return ['success' => false  ,'messages' => $ex->getMessageBag(), 'messages_info' => null];
            }                            
        }     
    }

    public function delete($id)
    {
        try{

            $delete = $this->repository->delete($id);

            if(!$delete)
                return [
                    'success'       => false,
                    'messages'      => 'Erro ao tentar excluir os dados.',
                    'messages_info' => null,
                ];

            return [
                'success'       => true,
                'messages'      => 'Dados excluidos com sucesso.',
                'messages_info' => null,
            ];

        }catch(Exception $ex){
            switch(get_class($ex)){
                case ValidatorException::class  : return ['success' => false  ,'messages' => $ex->getMessageBag(), 'messages_info' => null];
            }                            
        }
    }

    public function search($data)
    {
        $type           = (is_numeric($data['search'])) ? "registration"  : "name";
        
        if($type == "registration"){
            $professores = $this->repository->scopeQuery(function($query) use($data){
                return $query->where('registration', $data['search'])
                            ->where('type', 'Professor')
                            ->orderBy('name', 'asc');
            });
        }else{
            $professores = $this->repository->scopeQuery(function($query) use($data){
                return $query->where('name', 'LIKE', '%'.$data['search'].'%')
                            ->where('type', 'Professor')
                            ->orderBy('name', 'asc');
            });
        }

        return $professores;
    }

}