<?php 

namespace App\Services;

use App\Entities\Reserve;
use App\Repositories\BookRepository;
use App\Repositories\ReserveRepository;
use App\Services\AluguelService;
use App\Validators\ReservaValidator;
use Exception;

class ReservaService
{
	protected $reserve;
	protected $validator;
	protected $reserveRepository;
	protected $bookRepository;
	protected $aluguelService;
	protected $perPage;

	public function __construct(ReserveRepository $reserveRepository, ReservaValidator $validator, AluguelService $aluguelService, Reserve $reserve, BookRepository $bookRepository)
	{
		$this->validator 			= $validator;
		$this->reserveRepository 	= $reserveRepository;
		$this->perPage				= 15;
		$this->aluguelService		= $aluguelService;
		$this->reserve 				= $reserve;
		$this->bookRepository		= $bookRepository;
	}

	public function list()
	{
		$reservations = $this->reserveRepository->scopeQuery(function($query){
			return $query->join('users', 'users.id', '=', 'reserves.user_id')
						->join('books', 'books.id', '=', 'reserves.book_id')
						->select('users.name', 'users.id', 'books.title', 'reserves.*')
						->where('status', 'Reservado');
		});

		return $reservations->paginate($this->perPage);
	}

	public function checkTypeSearch(array $data)
	{
		$data['search'] = ($data['search'] != "") ? $data['search'] : "";

		if(is_numeric($data['search']) && $data['filter'] == 'aluno'){
			return 'aluno-registration';
		}

		if(!is_numeric($data['search']) && $data['filter'] == 'aluno'){
			return 'aluno-name';
		}

		if(is_numeric($data['search']) && $data['filter'] == 'professor'){
			return 'professor-registration';
		}

		if(!is_numeric($data['search']) && $data['filter'] == 'professor'){
			return 'professor-name';
		}

		if($data['filter'] == "livro"){
			return 'livro';
		}
	}

	public function search(array $data)
	{	
		$type = $this->checkTypeSearch($data);
		$data['search'] = ($data['search'] != null) ? $data['search'] : "";
		switch ($type) {
			case 'aluno-registration':
				$reservations = $this->reserveRepository->scopeQuery(function($query) use($data){
					return $query->join('users', 'users.id', '=', 'reserves.user_id')
						->join('books', 'books.id', '=', 'reserves.book_id')
						->where('users.registration', $data['search'])
						->select('users.name', 'books.title', 'reserves.*')
						->where('users.type', 'Aluno')
						->where('status', 'Reservado');
				});
			break;

			case 'aluno-name':
				$reservations = $this->reserveRepository->scopeQuery(function($query) use($data){
				return $query->join('users', 'users.id', '=', 'reserves.user_id')
					->join('books', 'books.id', '=', 'reserves.book_id')
					->where('users.name', 'LIKE', '%'.$data['search'].'%')
					->select('users.name', 'books.title', 'reserves.*')
					->where('users.type', 'Aluno')
					->where('status', 'Reservado');
				});
			break;

			case 'professor-registration':
				$reservations = $this->reserveRepository->scopeQuery(function($query) use($data){
				return $query->join('users', 'users.id', '=', 'reserves.user_id')
					->join('books', 'books.id', '=', 'reserves.book_id')
					->select('users.name', 'books.title', 'reserves.*')
					->where('users.registration', $data['search'])
					->where('users.type', 'Professor')
					->where('status', 'Reservado');
				});
			break;

			case 'professor-name':
				$reservations = $this->reserveRepository->scopeQuery(function($query) use($data){
				return $query->join('users', 'users.id', '=', 'reserves.user_id')
					->join('books', 'books.id', '=', 'reserves.book_id')
					->select('users.name', 'books.title', 'reserves.*')
					->where('users.name', 'LIKE', '%'.$data['search'].'%')
					->where('users.type', 'Professor')
					->where('status', 'Reservado');
				});
			break;

			case 'livro':
				$reservations = $this->reserveRepository->scopeQuery(function($query) use($data){
				return $query->join('users', 'users.id', '=', 'reserves.user_id')
					->join('books', 'books.id', '=', 'reserves.book_id')
					->select('users.name', 'books.title', 'reserves.*')
					->where('books.title', 'LIKE', '%'.$data['search'].'%')
					->where('status', 'Reservado');
				});
			break;
		}

		return $reservations->paginate($this->perPage);
	}

	public function getDataReserve($id)
	{
		$reserve = $this->reserveRepository->scopeQuery(function($query) use($id){
			return $query->join('users', 'users.id', '=', 'reserves.user_id')
						->join('books', 'books.id', '=', 'reserves.book_id')
						->select('users.name', 'users.type', 'users.course', 'users.registration', 'users.id', 'books.title', 'books.category', 'reserves.*')
						->where('reserves.id', $id)
						->where('status', 'Reservado');
		});
		return $reserve->first();
	}

	public function saveRent(array $data)
	{
		try{

			$reserve = $this->reserveRepository->find($data['reserve_id']);

			$checkStatus =
			$this->reserveRepository->scopeQuery(function($query)
			use($reserve){     return $query->where('user_id',
			$reserve['user_id'])     ->where('status', 'Alugado'); });

			if(count($checkStatus->all()) > 0){
				return [
					'success' 		=> false,
					'messages'		=> "Esse usuário já possui um livro alugado",
					'messages_info' => "Erro Aluguel",
				];
			}

			$data['status'] = "Alugado";
			$update			= $this->reserveRepository->update($data, $data['reserve_id']);

			return [
				'success'		=> true,
				'messages'		=> "Aluguel realizado com sucesso",
				'messages_info' => null,
			];										
		}catch(Exception $ex){
			switch(get_class($ex)){
				case QueryException::class      : return ['success' => false  ,'messages' => $ex->getMessage()];
                case ValidatorException::class  : return ['success' => false  ,'messages' => $ex->getMessageBag(), 'messages_info' => null];
                case Exception::class           : return ['success' => false  ,'messages' => $ex->getMessage()];
                default                         : return ['success' => false  ,'messages' => $ex->getMessage(), 'messages_info' => null];
			}
		}
	}

	public function reserveCancel(array $data)
	{
		try{
			$delete	= $this->reserveRepository->delete($data['reserve_id']);

			return [
				'success'		=> true,
				'messages'		=> "Reserva cancelada",
				'messages_info' => "A Reserva foi cancelada e agora você pode realizar a reserva de um novo livro",
			];										
		}catch(Exception $ex){
			switch(get_class($ex)){
				case QueryException::class      : return ['success' => false  ,'messages' => $ex->getMessage()];
                case ValidatorException::class  : return ['success' => false  ,'messages' => $ex->getMessageBag(), 'messages_info' => null];
                case Exception::class           : return ['success' => false  ,'messages' => $ex->getMessage()];
                default                         : return ['success' => false  ,'messages' => $ex->getMessage(), 'messages_info' => null];
			}
		}
	}

	public function bookReserve(array $data)
	{
		try{

			$checkUser	= $this->reserveRepository->scopeQuery(function($query) use($data){
				return $query->where('user_id', $data['user_id'])->where('status', 'Reservado')->orWhere('status', 'Alugado');
			});

			$hasBook 	= (count($checkUser->all()) == 1) ? true : false;
			
			if($hasBook){
				return [ 
					'success'		=> false,
					'messages'		=> "Você não pode efetuar a reserva de um novo livro!",
					'messages_info'	=> "Cancele sua reserva atual Ou Entregue o livro que foi alugado!",
				];
			}

			$book		 	 	= $this->bookRepository->find($data['book_id']);
			$amountInReserve 	= $this->reserve->checkBookAvailability($data['book_id']);
			$reservationAllowed = ($amountInReserve == $book->amount) ? false : true;

			if(!$reservationAllowed){
				return [ 
					'success'		=> false,
					'messages'		=> "Não temos mais nenhum exemplar do livro solicitado no nosso acervo.",
					'messages_info'	=> "Todos os exemplares do livro selecionado ou estão reservados ou alugados, tente novamente mais tarde.",
				];
			}

			$data['status'] 			= "Reservado";
			$data['reservation_date']	= $this->reserve->getDateNow();

			$reserve = Reserve::create($data);

			return [ 
				'success'		=> true,
				'messages'		=> "Reserva Realizada com sucesso!",
				'messages_info'	=> "Assim que possível passe na biblioteca para receber o livro.",
			];

		}catch(Exception $ex){
			switch(get_class($ex)){
				case QueryException::class      : return ['success' => false  ,'messages' => $ex->getMessage()];
                case ValidatorException::class  : return ['success' => false  ,'messages' => $ex->getMessageBag(), 'messages_info' => null];
                case Exception::class           : return ['success' => false  ,'messages' => $ex->getMessage()];
                default                         : return ['success' => false  ,'messages' => $ex->getMessage(), 'messages_info' => null];
			}
		}
	}

	public function allowCancellation($user_id, $reserve_id)
	{	
		$check = $this->reserveRepository->scopeQuery(function($query) use($user_id, $reserve_id){
			return $query->where('user_id', $user_id)->where('id', $reserve_id);
		});
		
		if(count($check->all()) == 1)
			return true;
	}
}