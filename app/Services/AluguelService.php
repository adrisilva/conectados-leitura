<?php 

namespace App\Services;

use App\Entities\Reserve;
use App\Repositories\ReserveRepository;
use App\Repositories\UserRepository;
use App\Services\LivroService;
use App\Validators\AluguelValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use Exception;

class AluguelService
{
	protected $reserveRepository;
	protected $userRepository;
	protected $validator;
	protected $reserve;
	protected $livroService;
	protected $perPage;

	public function __construct(AluguelValidator $validator, UserRepository $userRepository, ReserveRepository $reserveRepository, Reserve $reserve, LivroService $livroService)
	{
		$this->reserveRepository = $reserveRepository;
		$this->userRepository	 = $userRepository;
		$this->validator 		 = $validator;
		$this->reserve 			 = $reserve;
		$this->livroService 	 = $livroService;
		$this->perPage    		 = 15;
	}

	public function geteDevolutionDates($category)
	{
		$bookType = $this->livroService->getBookType($category);

		if($bookType == "Normal")
			return [
				'now' 		 => $this->reserve->getDateNow(),
				'devolution' => $this->reserve->getDateDevolutionNormal(),
			];

		return [
			'now' 		 => $this->reserve->getDateNow(),
			'devolution' => $this->reserve->getDateDevolutionTechnician(),
		];
	}

	public function rentBook(array $data)
	{
		try{
			$user = $this->userRepository->findByField('registration', $data['registration'])->first();
			$data['user_id'] = $user->id;
			$data['status']  = "Alugado";

			$checkStatus = $this->reserveRepository->scopeQuery(function($query) use($data){
				return $query->where('user_id', $data['user_id'])
							->where('status', 'Alugado');
			});

			if(count($checkStatus->all()) > 0){
				return [
					'success' 		=> false,
					'messages'		=> "Esse usuário já possui um livro alugado",
					'messages_info' => "Erro Aluguel",
				];
			}
				
			$this->validator->setId($data['user_id']);
			$this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);

			$insert = $this->reserveRepository->create($data);

			return [
				'success' 		=> true,
				'messages'		=> "Aluguel Realizado com sucesso",
				'messages_info' => null,
			];

		}catch(Exception $ex){
			switch(get_class($ex)){
				//case QueryException::class      : return ['success' => false  ,'messages' => $ex->getMessage()];
                case ValidatorException::class  : return ['success' => false  ,'messages' => $ex->getMessageBag(), 'messages_info' => null];
                //case Exception::class           : return ['success' => false  ,'messages' => $ex->getMessage()];
                //default                         : return ['success' => false  ,'messages' => $ex->getMessage(), 'messages_info' => null];
			}
		}
	}

	public function list()
	{
		$rents = $this->reserveRepository->scopeQuery(function($query){
			return $query->join('users', 'users.id', '=', 'reserves.user_id')
						->join('books', 'books.id', '=', 'reserves.book_id')
						->select('users.name', 'books.title', 'reserves.*')
						->where('status', 'Alugado');
		});

		return $rents->paginate($this->perPage);
	}

	public function checkTypeSearch($data)
	{
		$data['search'] = ($data['search'] != "") ? $data['search'] : "";

		if(is_numeric($data['search']) && $data['filter'] == 'aluno'){
			return 'aluno-registration';
		}

		if(!is_numeric($data['search']) && $data['filter'] == 'aluno'){
			return 'aluno-name';
		}

		if(is_numeric($data['search']) && $data['filter'] == 'professor'){
			return 'professor-registration';
		}

		if(!is_numeric($data['search']) && $data['filter'] == 'professor'){
			return 'professor-name';
		}

		if($data['filter'] == "livro"){
			return 'livro';
		}
	}

	public function search($data)
	{	
		$type = $this->checkTypeSearch($data);
		$data['search'] = ($data['search'] != null) ? $data['search'] : "";
		switch ($type) {
			case 'aluno-registration':
				$rents = $this->reserveRepository->scopeQuery(function($query) use($data){
					return $query->join('users', 'users.id', '=', 'reserves.user_id')
						->join('books', 'books.id', '=', 'reserves.book_id')
						->where('users.registration', $data['search'])
						->where('users.type', 'Aluno')
						->where('status', 'Alugado');
				});
			break;

			case 'aluno-name':
				$rents = $this->reserveRepository->scopeQuery(function($query) use($data){
				return $query->join('users', 'users.id', '=', 'reserves.user_id')
					->join('books', 'books.id', '=', 'reserves.book_id')
					->where('users.name', 'LIKE', '%'.$data['search'].'%')
					->select('users.name', 'books.title', 'reserves.*')
					->where('users.type', 'Aluno')
					->where('status', 'Alugado');
				});
			break;

			case 'professor-registration':
				$rents = $this->reserveRepository->scopeQuery(function($query) use($data){
				return $query->join('users', 'users.id', '=', 'reserves.user_id')
					->join('books', 'books.id', '=', 'reserves.book_id')
					->select('users.name', 'books.title', 'reserves.*')
					->where('users.registration', $data['search'])
					->where('users.type', 'Professor')
					->where('status', 'Alugado');
				});
			break;

			case 'professor-name':
				$rents = $this->reserveRepository->scopeQuery(function($query) use($data){
				return $query->join('users', 'users.id', '=', 'reserves.user_id')
					->join('books', 'books.id', '=', 'reserves.book_id')
					->select('users.name', 'books.title', 'reserves.*')
					->where('users.name', 'LIKE', '%'.$data['search'].'%')
					->where('users.type', 'Professor')
					->where('status', 'Alugado');
				});
			break;

			case 'livro':
				$rents = $this->reserveRepository->scopeQuery(function($query) use($data){
				return $query->join('users', 'users.id', '=', 'reserves.user_id')
					->join('books', 'books.id', '=', 'reserves.book_id')
					->select('users.name', 'books.title', 'reserves.*')
					->where('books.title', 'LIKE', '%'.$data['search'].'%')
					->where('status', 'Alugado');
				});
			break;
		}

		return $rents->paginate($this->perPage);
	}

	public function returnBook(array $data)
	{
		try{
			$data['return_date'] = $this->reserve->getDateNow();
			$data['status'] 	 = "Entregue";
			$update 			 =	$this->reserveRepository->update($data, $data['reserve_id']);

			if($update)
				return [
					'success'  		=> true,
					'messages' 		=> 'Devolução Realizada Com Sucesso',
					'messages_info' => null,
				];
			
		}catch(Exception $ex){
			switch(get_class($ex)){
				//case QueryException::class      : return ['success' => false  ,'messages' => $ex->getMessage()];
                case ValidatorException::class  : return ['success' => false  ,'messages' => $ex->getMessageBag(), 'messages_info' => null];
                //case Exception::class           : return ['success' => false  ,'messages' => $ex->getMessage()];
                //default                         : return ['success' => false  ,'messages' => $ex->getMessage(), 'messages_info' => null];
			}
		}
	}

	public function renewBook($id)
	{

		$reserve = $this->reserveRepository->scopeQuery(function($query) use($id){
			return $query->join('users', 'users.id', '=', 'reserves.user_id')
						->join('books', 'books.id', '=', 'reserves.book_id')
						->select('books.title','books.category', 'reserves.id', 'users.name', 'users.registration', 'users.course')
						->where('reserves.id', $id);
		});

		$data = $reserve->first();
	
		return $data;
	}

	public function completeRenovation(array $data)
	{
		try{
			$update =	$this->reserveRepository->update($data, $data['reserve_id']);

			if($update)
				return [
					'success'  		=> true,
					'messages' 		=> 'Renovação realizada com sucesso',
					'messages_info' => null,
				];
			
		}catch(Exception $ex){
			switch(get_class($ex)){
				//case QueryException::class      : return ['success' => false  ,'messages' => $ex->getMessage()];
                case ValidatorException::class  : return ['success' => false  ,'messages' => $ex->getMessageBag(), 'messages_info' => null];
                //case Exception::class           : return ['success' => false  ,'messages' => $ex->getMessage()];
                //default                         : return ['success' => false  ,'messages' => $ex->getMessage(), 'messages_info' => null];
			}
		}
	}

}
