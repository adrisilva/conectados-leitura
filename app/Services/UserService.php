<?php 

namespace App\Services;

use App\Repositories\ReserveRepository;
use App\Repositories\UserRepository;
use App\Validators\UserValidator;
use Exception;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

class UserService
{

	protected $repository;
	protected $validator;
	protected $reserveRepository;

	public function __construct(UserRepository $repository, UserValidator $validator, ReserveRepository $reserveRepository)
	{
		$this->repository   		= $repository;
		$this->validator   			= $validator;
		$this->reserveRepository 	= $reserveRepository;
	}

	public function changePassword(array $data)
	{
		try{
			$this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);
			$data['password'] = bcrypt($data['new_password']);
			$update = $this->repository->update($data, $data['user_id']);
			return [
				'success' 		=> true,
				'messages'		=> 'Sucesso!',
				'messages_info'	=> 'A sua nova senha foi salva com sucesso.',
			];
		}catch(Exception $ex){
			switch(get_class($ex))
            {
                case QueryException::class      : return ['success' => false  ,'messages' => $ex->getMessage()];
                case ValidatorException::class  : return ['success' => false  ,'messages' => $ex->getMessageBag(), 'messages_info' => null];
                case Exception::class           : return ['success' => false  ,'messages' => $ex->getMessage()];
                default                         : return ['success' => false  ,'messages' => $ex->getMessage(), 'messages_info' => null];
            }
		}
	}

	public function getUserHistoric($user_id)
	{
		//$historic = $this->reserveRepository->findByField('user_id', $user_id);
		$historic = $this->reserveRepository->scopeQuery(function($query) use($user_id){
			return $query->join('books', 'books.id', '=', 'reserves.book_id')
						->select('books.title', 'books.id', 'reserves.*')
						->where('user_id', $user_id);
		});
		return $historic->all();
	}

}