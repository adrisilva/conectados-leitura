<?php 

namespace App\Services;

use App\Entities\Recommendation;
use App\Entities\School;
use App\Repositories\RecomedationRepository;
use App\Validators\RecomendacaoValidator;
use Auth;
use Carbon\Carbon;
use Exception;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

class RecomendacaoService
{
	protected $repository;
	protected $validator;

	public function __construct(RecomedationRepository $repository, RecomendacaoValidator $validator)
	{
		$this->repository = $repository;
		$this->validator  = $validator;
	}

	public function recommendBook($data)
	{
		try{
		    $school = School::where('slug', request()->school)->first();
			$this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);

			$bookRecomendado = Recommendation::where('book_id', $data['book_id'])->where('school_id', $school->id)->get();

			if(count($bookRecomendado) > 0){
                return [
                    'success' 		=> false,
                    'messages'		=> "Aviso",
                    'messages_info'	=> "Esse livro já foi recomendado!",
                ];
            }

			$data['recommendation_date'] = new Carbon();
			$data['user_id']			 = Auth::user()->id;
			$data['school_id']              = $school->id;
			$insert 					 = $this->repository->create($data);
			return [
				'success' 		=> true,
				'messages'		=> "Recomendação realizada com sucesso",
				'messages_info'	=> "Obrigado pela recomendação!",
			];
		}catch(Exception $ex){
		  switch(get_class($ex))
            {
                case ValidatorException::class  : return ['success' => false  ,'messages' => $ex->getMessageBag(), 'messages_info' => null];
            }
		}
	}

	public static function listRecomendations()
    {
        $school = School::where('slug', request()->school)->first();

        $recomendations = Recommendation::where('school_id', $school->id)->get();

        return $recomendations;
    }
}