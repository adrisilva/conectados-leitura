<?php

namespace App\Services;

use Intervention\Image\ImageManagerStatic as Image;
use App\Repositories\AuthorRepository;
use App\Repositories\BookRepository;
use App\Validators\LivroValidator;
use Exception;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

class LivroService
{
	protected $repository;
	protected $validator;
	protected $perPage;
	protected $authorRepository;

	public function __construct(LivroValidator $validator, BookRepository $repository, AuthorRepository $authorRepository)
	{
		$this->repository 		= $repository;
		$this->authorRepository = $authorRepository;
		$this->validator  		= $validator;
		$this->perPage    		= 24;
	}

	public function list()
	{
		$livros = $this->repository->scopeQuery(function($query){
			return $query->join('authors', 'authors.book_id', '=', 'books.id')
						->select('books.*', 'authors.name')
						->orderBy('title', 'asc');
		});
		return $livros->paginate($this->perPage);
	}

	public function search(array $data)
	{
		$livros = $this->repository->scopeQuery(function($query) use($data){

			if($data['filter'] == 'book_title' && $data['category'] == "Todos")
				return $query->join('authors', 'authors.book_id', '=', 'books.id')
							->select('books.*', 'authors.name')
							->where('title', 'LIKE', '%'.$data['search'].'%')
							->orderBy('title', 'asc');

			if($data['filter'] == 'author_name' && $data['category'] == "Todos")
				return $query->join('authors', 'authors.book_id', '=', 'books.id')
						->select('books.*', 'authors.name')
						->where('authors.name' ,'LIKE', '%'.$data['search'].'%')
						->orderBy('title', 'asc');

			if($data['filter'] == 'book_title' && $data['category'] != "Todos")
				return $query->join('authors', 'authors.book_id', '=', 'books.id')
							->select('books.*', 'authors.name')
							->where('title', 'LIKE', '%'.$data['search'].'%')
							->where('category', $data['category'])
							->orderBy('title', 'asc');

			if($data['filter'] == 'author_name' && $data['category'] != "Todos")
				return $query->join('authors', 'authors.book_id', '=', 'books.id')
						->select('books.*', 'authors.name')
						->where('authors.name' ,'LIKE', '%'.$data['search'].'%')
						->where('category', $data['category'])
						->orderBy('title', 'asc');
		});
		return $livros->paginate($this->perPage);
	}

	public function store($request)
	{
		try{
			$data = $request->all();
			$this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);
				
			$name 			= kebab_case($data['title']).date('hms');
			$extension 		= $data['cover']->extension();
			$filename 		= "{$name}.{$extension}";
			$data['cover']	= $filename;
			$path			= public_path('covers/'.$filename);
			$cover 			= Image::make($request->file('cover'))->fit(340, 480);
			$upload 		= $cover->save($path);

			if(!$upload)
				return [
					"success" 		=> false,
					"messages"		=> "Erro ao tentar realizar o cadastro",
					"messages_info"	=> null,
				];
		
			$livro  	= $this->repository->create($data);
			$dataAuthor = [ 'book_id' => $livro->id, 'name' => $data['author']];
			$author 	= $this->authorRepository->create($dataAuthor);
			return [
				"success" 		=> true,
				"messages"		=> "Cadastro realizado com sucesso.",
				"messages_info"	=> null,
			];

		}catch(Exception $ex){
			switch(get_class($ex))
            {
                //case QueryException::class      : return ['success' => false  ,'messages' => $ex->getMessage()];
                case ValidatorException::class  : return ['success' => false  ,'messages' => $ex->getMessageBag(), 'messages_info' => null];
                //case Exception::class           : return ['success' => false  ,'messages' => $ex->getMessage()];
                //default                         : return ['success' => false  ,'messages' => $ex->getMessage(), 'messages_info' => null];
            }
		}
	}

	public function update($request, $id)
	{
		try{	
			$data = $request->all();
			$this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);

			if(isset($data['cover'])){
				$name 			= kebab_case($data['title']).date('hms');
				$extension 		= $data['cover']->extension();
				$filename 		= "{$name}.{$extension}";
				$data['cover']	= $filename;
				$path			= public_path('covers/'.$filename);
				
				$cover 			= Image::make($request->file('cover'))->fit(340, 480);
				$upload 		= $cover->save($path);

				if(!$upload)
					return [
						"success" 		=> false,
						"messages"		=> "Erro ao tentar atualizar os dados.",
						"messages_info"	=> null,
					];
			}

			$livro  	= $this->repository->update($data, $id);
			$dataAuthor = ['book_id' => $livro->id, 'name' => $data['author']];
			$author 	= $this->authorRepository->update($dataAuthor, $data['author_id']);

			return [
				"success" 		=> true,
				"messages"		=> "Dados Atualizados com sucesso.",
				"messages_info"	=> null,
			];

		}catch(Exception $ex){
			switch(get_class($ex))
            {                
                case ValidatorException::class  : return ['success' => false  ,'messages' => $ex->getMessageBag(), 'messages_info' => null];
            }
		}
	}

	public function delete($id)
	{
		try{
			$deleteLivro = $this->repository->delete($id);

			if(!$deleteLivro)
				return [
					'success'  		=> false,
					'messages' 		=> "Erro ao tentar excluir os dados.",
					'messages_info' => null,
				];

			return [
				'success'  		=> true,
				'messages' 		=> "Dados excluidos com sucesso.",
				'messages_info' => null,
			];

		}catch(Exception $ex){
            switch(get_class($ex))
            {
                //case QueryException::class      : return ['success' => false  ,'messages' => $ex->getMessage()];
                //case ValidatorException::class  : return ['success' => false  ,'messages' => $ex->getMessageBag(), 'messages_info' => null];
                case Exception::class           : return ['success' => false  ,'messages' => $ex->getMessage()];
                //default                         : return ['success' => false  ,'messages' => $ex->getMessage(), 'messages_info' => null];
            }
        }
	}

	public function getBookType($category)
	{
		switch ($category) {
			case 'Enfermagem': return 'Técnico'; break;
			case 'Informática': return 'Técnico'; break;
			case 'Finanças': return 'Técnico'; break;
			case 'Moda': return 'Técnico'; break;	
			default: return 'Normal'; break;
		}
	}

	public function show($id)
	{
		$livro = $this->repository->scopeQuery(function($query) use($id){
			return $query->join('authors', 'authors.book_id', '=', 'books.id')
				->select('books.*', 'authors.name')
				->where('books.id', $id);
		});
		return $livro->first();
	}
}