<?php 

namespace App\Services;

use App\Entities\Book;
use App\Repositories\AuthorRepository;

class AutorService
{
	protected $repository;

	public function __construct(AuthorRepository $repository)
	{
		$this->repository = $repository;
	}

	public function list()
	{
		$livros = $this->repository->with(['books'])->get();
		return $livros;
	}



}