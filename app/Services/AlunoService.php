<?php 

namespace App\Services;

use App\Repositories\UserRepository;
use App\Validators\AlunoValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use Exception;

class AlunoService
{
    private $repository;
    private $validator;
    private $perPage;

    public function __construct(UserRepository $repository, AlunoValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->perPage    = 20;
    }

    public function store(array $data)
    {
        
        try{
            $password = generatePassword();

            $data['password'] = bcrypt($password);

            if($data['school_type'] != 'eeep')
                $data['course'] = "-";

            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE); 

            $aluno = $this->repository->create($data);

            if($aluno){
                $data['password'] = $password;
                sendEmailPassword($data);
            }

            return [
                'success' => true,
                'messages' => "Cadastro realizado com sucesso.",
                'messages_info' => "A senha de acesso da conta do aluno foi enviada para o e-mail do mesmo.",
            ];

        }catch(Exception $ex){

            switch(get_class($ex))
            {
                //case QueryException::class      : return ['success' => false  ,'messages' => $ex->getMessage()];
                case ValidatorException::class  : return ['success' => false  ,'messages' => $ex->getMessageBag(), 'messages_info' => null];
                //case Exception::class           : return ['success' => false  ,'messages' => $ex->getMessage()];
                default                         : return ['success' => false  ,'messages' => $ex->getMessage(), 'messages_info' => null];
            }

        }
    }

    public function update(array $data, $id)
    {
        try{

            $this->validator->setId($id);
            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE); 
            
            if($data['password'] != null)
                $data['password'] = bcrypt($data['password']);
            else
                unset($data['password']);

            $usuario = $this->repository->update($data, $id);

            return [
                'success' => true,
                'messages' => "Dados atualizados com sucesso.",
                'messages_info' => null,
            ];

        }catch(Exception $ex){

            switch(get_class($ex))
            {
                //case QueryException::class      : return ['success' => false  ,'messages' => $ex->getMessage()];
                case ValidatorException::class  : return ['success' => false  ,'messages' => $ex->getMessageBag(), 'messages_info' => null];
                //case Exception::class           : return ['success' => false  ,'messages' => $ex->getMessage()];
                //default                         : return ['success' => false  ,'messages' => $ex->getMessage(), 'messages_info' => null];
            }
        }
    }

    public function delete($id)
    {
        try{

            $delete = $this->repository->delete($id);
            
            if(!$delete)
                return [
                    'success' => true,
                    'messages' => "Erro ao tentar excluir os dados.",
                    'messages_info' => null,
                ];


            return [
                'success' => true,
                'messages' => "Dados excluidos com sucesso.",
                'messages_info' => null,
            ];

        }catch(Exception $ex){

            switch(get_class($ex))
            {
                //case QueryException::class      : return ['success' => false  ,'messages' => $ex->getMessage()];
                case ValidatorException::class  : return ['success' => false  ,'messages' => $ex->getMessageBag(), 'messages_info' => null];
                //case Exception::class           : return ['success' => false  ,'messages' => $ex->getMessage()];
                //default                         : return ['success' => false  ,'messages' => $ex->getMessage(), 'messages_info' => null];
            }
        }
    }

    public function search(array $data)
    {
        $type           = (is_numeric($data['search'])) ? "registration"  : "name";
        $search         = (!isset($data['search']))     ? ""              : $data['search'];
        $data['course'] = (isset($data['course']))      ? $data['course'] : null;

        if($data['course'] != null && $type == "name"){
            $alunos = $this->repository->scopeQuery(function($query) use($data){
                    return $query->where('name', 'LIKE', '%'.$data['search'].'%')
                                ->where('course', $data['course'])
                                ->where('type', 'aluno')
                                ->orderBy('name', 'asc');                         
            });
        }

        if($data['course'] == null && $type == "name"){
            $alunos = $this->repository->scopeQuery(function($query) use($data){
                return $query->where('name', 'LIKE', '%'.$data['search'].'%')
                            ->where('type', 'aluno')
                            ->orderBy('name', 'asc');
            });
        }

        if($data['course'] != null && $type == "registration"){
            $alunos = $this->repository->scopeQuery(function($query) use($data){
                return $query->where('registration', $data['search'])
                            ->where('course', $data['course'])
                            ->where('type', 'aluno')
                            ->orderBy('name', 'asc');
            });
        }

        if($data['course'] == null && $type == "registration"){
            $alunos = $this->repository->scopeQuery(function($query) use($data){
                return $query->where('registration', $data['search'])
                            ->where('type', 'aluno')
                            ->orderBy('name', 'asc');
            });
        }

        return $alunos;
    }


    public function list()
    {
        $alunos = $this->repository->scopeQuery(function($query){
            return $query->where('type', 'Aluno')
                        ->orderBy('name', 'asc');                          
        });

        return $alunos->paginate($this->perPage);
    }

    public function countStudents()
    {
        $alunos = $this->repository->scopeQuery(function($query){
            return $query->where('type', 'Aluno');                          
        });

        dd($alunos->count());
    }
    
}
