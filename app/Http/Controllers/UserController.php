<?php

namespace App\Http\Controllers;

use App\Entities\Recommendation;
use App\Entities\School;
use App\Entities\User;
use App\Services\AvaliacaoService;
use App\Services\ReservaService;
use App\Services\UserService;
use Auth;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $service;
    protected $reservaService;
    protected $avaliacaoService;

    public function __construct(UserService $service, ReservaService $reservaService, AvaliacaoService $avaliacaoService)
    {
//    	$this->middleware('auth');
    	$this->service          = $service;
        $this->reservaService   = $reservaService;
        $this->avaliacaoService = $avaliacaoService;
    }

    public function changePassword(Request $request, $school)
    {
    	$data     			= $request->except('_token');
    	$data['user_id']	= Auth::user()->id;
    	$response 			= $this->service->changePassword($data);

    	if($response['success'] && $request->user()->first_access){
            $user = $request->user();
            $user->first_access = 0;
            $user->save();
        }

    	session()->flash('success', [
    		'success' 		=> $response['success'],
    		'messages'		=> $response['messages'],
    		'messages_info'	=> $response['messages_info'],
    	]);

    	if(!$response['success'])
    		return redirect('/')->withError($response['messages']);

    	return redirect('/');
    }

    public function showHistoric(UserService $service, $school)
    {
        $user_id  = Auth::user()->id; 
        $historic = $service->getUserHistoric($user_id);
        return view('site.historico', compact('historic', $school));
    }

    public function showRecomendations($slug)
    {
        $school = School::where('slug', $slug)->first();
        $recomendations = Recommendation::where('school_id', $school->id)
                                ->where('user_id', Auth::user()->id)
                                ->get();

        return view('site.recomendations', compact('recomendations'));
    }

    public function reserveCancel(Request $request, $school)
    {
        $data = $request->except(['_token', '_method']);
        $user_id = Auth::user()->id;

       /* $check = $this->reservaService->allowCancellation($user_id, $data['reserve_id']);

        if(!$check)
            return redirect()->route('site.index', $school);*/
            
        $response = $this->reservaService->reserveCancel($data);

        session()->flash('success', [
            'success'       => $response['success'],
            'messages'      => $response['messages'],
            'messages_info' => $response['messages_info'],
        ]);

        if(!$response['success'])
            return redirect('/')->withError($response['messages']);

        return redirect('/');
    }

    public function bookRating(Request $request, $school)
    {
        $data            = $request->except('_token');
        $data['user_id'] = Auth::user()->id;
        $response        = $this->avaliacaoService->bookRating($data);

        session()->flash('success', [
            'success'       => $response['success'],
            'messages'      => $response['messages'],
            'messages_info' => $response['messages_info'],
        ]);

        return redirect()->back();
    }

    public function firstAccess()
    {
        return view('auth.change');
    }

    public function firstAccessAdmin()
    {
        return view('auth.change_admin');
    }

    public function changePasswordAdmin(Request $request, $school)
    {
        $user = \Illuminate\Support\Facades\Auth::guard('admin')->user();
        $user->password = bcrypt($request->new_password);
        $user->first_access = 0;
        $user->save();

        return redirect('admin/dashboard');
    }
}
