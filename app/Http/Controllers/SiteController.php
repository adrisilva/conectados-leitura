<?php

namespace App\Http\Controllers;

use App\Services\RecomendacaoService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Auth;

class SiteController extends Controller
{
    public function index()
    {
        $recomendacoes = RecomendacaoService::listRecomendations();
        return view('site.index', compact('recomendacoes'));
    }

    public function sobre()
    {
        return view('site.sobre');
    }

    public function contato()
    {
        return view('site.contato');
    }

    public function livros($categoria)
    {
        return view('site.livros', compact('categoria'));
    }

    public function loginForm()
    {
        return view('admin.login');
    }

}
