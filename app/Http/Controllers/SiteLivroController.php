<?php

namespace App\Http\Controllers;

use App\Entities\Author;
use App\Entities\Book;
use App\Entities\School;
use App\Entities\User;
use App\Repositories\BookRepository;
use App\Services\LivroService;
use App\Services\ReservaService;
use Illuminate\Http\Request;
use Auth;

class SiteLivroController extends Controller
{
	protected $categories;
	protected $service;
	protected $repository;
    protected $reserveService;

	public function __construct(LivroService $service, BookRepository $repository, ReservaService $reserveService)
	{
		$this->repository     = $repository;
		$this->service        = $service;
		$this->categories     = [
			'Todos'				=>  'Todos',
			'Artes' 			=>	'Artes',
			'Espanhol' 			=>	'Espanhol',
			'EducaçãoFísica' 	=>	'Educação Física',
			'História' 			=>	'História',
			'Inglês' 			=>	'Inglês',
			'Literatura'		=>	'Literatura',
			'Música' 			=>	'Música',
			'Enfermagem' 		=>	'Enfermagem',
			'Finanças' 			=>	'Finanças',
			'Informática' 		=>	'Informática',
			'Moda' 				=>	'Moda',
		];
        $this->reserveService = $reserveService;
	}

    public function list($school, $categoria)
    {
        $categories         	= $this->categories;
        $data           		= null;
        $dataForm['category']   = $categoria;
        $dataForm['filter']     = 'book_title';
        $dataForm['search']     = (!isset($dataForm['search']) ? "" : $dataForm['search']);

        if($categoria == "Todos")
            $livros = $this->service->list();
        else
            $livros = $this->service->search($dataForm);

        return view('site.livros', compact('categoria', 'categories', 'data', 'livros', 'school'));
    }

     public function search(Request $request, $school)
    {
    	$categories 	= $this->categories;
    	$data 			= $request->except('_token');
    	$data['search'] = (!isset($data['search']) ? "" : $data['search']);
    	$livros 		= $this->service->search($data);
    	return view('site.livros', compact('categories', 'data', 'livros', 'school'));
    }

    public function show($school, $id)
    {
    	$livro = $this->service->show($id);
    	return response()->json($livro);
    }

    public function bookReserve(Request $request, $school)
    {
        $data            = $request->except('_token');
        $data['user_id'] = Auth::user()->id;
        $response        = $this->reserveService->bookReserve($data);

        session()->flash('success', [
            'success'       => $response['success'],
            'messages'      => $response['messages'],
            'messages_info' => $response['messages_info'],
        ]);

        return redirect('/')->withErrors($response['messages']);
    }
}
