<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Reserve;
use App\Http\Controllers\Controller;
use App\Repositories\BookRepository;
use App\Repositories\ReserveRepository;
use App\Repositories\UserRepository;
use App\Services\AluguelService;
use Illuminate\Http\Request;

class RentController extends Controller
{
   	protected $service;
	protected $reserveRepository;
   	protected $bookRepository;
	protected $userRepository;
	protected $reserve;

	public function __construct(BookRepository $bookRepository, UserRepository $userRepository, ReserveRepository $reserveRepository, AluguelService $service)
	{
  		$this->middleware('auth:admin');
		$this->bookRepository    = $bookRepository;
		$this->userRepository    = $userRepository;
		$this->reserveRepository = $reserveRepository;
		$this->service			 = $service;
	}

	public function exibirFormularioAluguel($school, $id)
	{
		$courses    =  [
         'Enfermagem'        => 'Enfermagem', 
         'Finanças'          => 'Finanças',
         'Informática'       => 'Informática',
         'Produção de Moda'  => 'Produção de Moda'
  		];
      	
      	$livro = $this->bookRepository->find($id);
      	$category = $livro->category;
  		$data  = $this->service->geteDevolutionDates($category);

		return view('admin.alugueis.aluguel', compact('courses', 'data', 'livro'));
	}

	public function rent(Request $request, $school)
	{
		$data 	  	= $request->except('_token');
		$response 	= $this->service->rentBook($data); 
		session()->flash('success', [
			'success' 		=> $response['success'],
			'messages'		=> $response['messages'],
			'messages_info'	=> $response['messages_info'],
		]);

		if(!$response['success'])
            return redirect()->back();

        return redirect('admin/dashboard');
	}

	public function list($school)
	{
		$rents = $this->service->list();
		return view('admin.alugueis.listar', compact('rents', 'school'));
	}

	public function search(Request $request, $school)
	{
		$data = $request->except('_token');
		$rents = $this->service->search($data);
		return view('admin.alugueis.listar', compact('rents', 'data', 'school'));
	}

	public function returnBook(Request $request, $school)
	{
		$data = $request->except('_token');
		$response = $this->service->returnBook($data);

		session()->flash('success', [
			'success' 		=> $response['success'],
			'messages'		=> $response['messages'],
			'messages_info'	=> $response['messages_info'],
		]);

		return redirect()->back();
	}

	public function renewBook($school, $id)
	{
		$data = $this->service->renewBook($id);
		$dates = $this->service->geteDevolutionDates($data->category);
		return view('admin.alugueis.renovacao', compact('data', 'dates', 'school'));
	}

	public function completeRenovation(Request $request, $school)
	{
		$data = $request->all();
		$response = $this->service->completeRenovation($data);

		session()->flash('success', [
			'success' 		=> $response['success'],
			'messages'		=> $response['messages'],
			'messages_info'	=> $response['messages_info'],
		]);

        return redirect('admin/dashboard');
	}
}
