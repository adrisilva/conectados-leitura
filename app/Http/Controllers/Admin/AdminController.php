<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Book;
use App\Entities\Reserve;
use App\Entities\User;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class AdminController extends Controller
{
	protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->middleware('auth:admin');
        $this->userRepository = $userRepository;
    }

    public function index(User $user, Reserve $reserve, Book $book, $school)
    {
      $students     = $user->countStudents();
      $books        = $book->countBooks();
      $reserves     = $reserve->countReserves();
      $rents        = $reserve->countRents();
      $rentsToday   = $reserve->countRentsToday();
      $rentsExpired = $reserve->countRentsExpired();

      return view('admin.dashboard', compact('students', 'books', 'reserves', 'rents', 'rentsToday', 'rentsExpired', 'school'));
    }

   	public function showUser($school, $origin, $id)
   	{
   		$user = $this->userRepository->find($id);
   		return view('admin.usuarios.user', compact('user', 'origin', 'school'));
   	}
}
