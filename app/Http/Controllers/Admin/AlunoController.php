<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use App\Services\AlunoService;

class AlunoController extends Controller
{
    protected $repository;
    protected $service;
    protected $courses;
    protected $series;


    public function __construct(UserRepository $repository, AlunoService $service)
    {
        $this->middleware('auth:admin');
        $this->repository = $repository;
        $this->service    = $service;
        $this->courses    =  [
            'Enfermagem'        => 'Enfermagem', 
            'Finanças'          => 'Finanças',
            'Informática'       => 'Informática',
            'Produção de Moda'  => 'Produção de Moda'
        ];

        $this->series     = [
            '7' => '7º Ano',
            '8' => '8º Ano',
            '9' => '9º Ano',
            '1' => '1º Ano do Ensino Médio',
            '2' => '2º Ano do Ensino Médio',
            '3' => '3º Ano do Ensino Médio',
        ];
    }

    public function exibirFormularioCadastro($school)
    {
        $courses = $this->courses;
        $series  = $this->series;
        return view('admin.alunos.cadastro-editar', compact('courses', 'series', 'school'));
    }

    public function list($school)
    {
        $alunos     = $this->service->list();
        $data       = null;
        $courses    = $this->courses;
        return view('admin.alunos.listar', compact('alunos', 'data', 'courses', 'school'));
    }

    public function store(Request $request, $school)
    {
        $response = $this->service->store($request->all());

        session()->flash('success', [
            'success'       => $response['success'],
            'messages'      => $response['messages'],
            'messages_info' => $response['messages_info'],
        ]);

        if($response['success']){
            return redirect()->route('admin.dashboard', $school);
        }
        
        return redirect()->back()->withErrors($response['messages'])->withInput();
    }

    public function edit($school, $id)
    {
        $text       = "Editando Dados";
        $aluno      = $this->repository->find($id);
        $courses    = $this->courses;
        $series     = $this->series;
        return view('admin.alunos.cadastro-editar', compact('aluno', 'text', 'courses', 'series', 'school'));
    }

    public function update(Request $request, $school, $id)
    {
        $response = $this->service->update($request->all(), $id);

        session()->flash('success', [
            'success'       => $response['success'],
            'messages'      => $response['messages'],
            'messages_info' => $response['messages_info'],
        ]);

        if($response['success']){
            return redirect()->route('aluno.listar', $school);
        }
        
        return redirect()->back()->withErrors($response['messages'])->withInput();
    }

    public function delete(Request $request, $school)
    {
        $data       = $request->all();
        $response   = $this->service->delete($data['id']);
        
        session()->flash('success', [
            'success'       => $response['success'],
            'messages'      => $response['messages'],
            'messages_info' => $response['messages_info'],
        ]);

        return redirect()->route('aluno.listar', $school);
    }

    public function search(Request $request, $school)
    {
        $data       = $request->all();
        $courses    = $this->courses;
        $alunos     = $this->service->search($data);
        $alunos     = $alunos->paginate(15);
        return view('admin.alunos.listar', compact('alunos', 'data', 'courses', 'school'));
    }

    public function searchLite(Request $request, $school)
    {
        $data    = $request->all();
        $alunos  = $this->service->search($data);
        return response()->json($alunos->all());
    }

}
