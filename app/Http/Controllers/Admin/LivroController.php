<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Book;
use App\Http\Controllers\Controller;
use App\Repositories\BookRepository;
use App\Services\AutorService;
use App\Services\LivroService;
use Illuminate\Http\Request;


class LivroController extends Controller
{
	protected $categories;
	protected $service;
	protected $repository;

	public function __construct(LivroService $service, BookRepository $repository)
	{
        $this->middleware('auth:admin');
        $this->repository = $repository;
		$this->service    = $service;
		$this->categories = [
			'Todos'				=>  'Todos',
			'Artes' 			=>	'Artes',
			'Espanhol' 			=>	'Espanhol',
			'EducaçãoFísica' 	=>	'Educação Física',
			'História' 			=>	'História',
			'Inglês' 			=>	'Inglês',
			'Literatura'		=>	'Literatura',
			'Música' 			=>	'Música',
			'Enfermagem' 		=>	'Enfermagem',
			'Finanças' 			=>	'Finanças',
			'Informática' 		=>	'Informática',
			'Moda' 				=>	'Moda',
			'Dicionário'		=> 	'Dicionário'
		];
	}

    public function list()
    {
    	$categories = $this->categories;
    	$livros     = $this->service->list();
    	$data       = null;
    	return view('admin.livros.listar', compact('categories', 'livros', 'data', 'school'));
    }

    public function search(Request $request, $school)
    {
    	$categories 	= $this->categories;
    	$data 			= $request->except('_token');
    	$data['search'] = (!isset($data['search']) ? "" : $data['search']);
    	$livros 		= $this->service->search($data);
    	return view('admin.livros.listar', compact('categories', 'livros', 'data'));
    }

    public function exibirFormularioCadastro()
    {
    	$categories = $this->categories;
    	unset($categories['Todos']);
    	return view('admin.livros.cadastrar-editar', compact('categories'));
    }

    public function store(Request $request, $school)
    {
    	$response = $this->service->store($request);

    	session()->flash('success', [
    		'success' 		=> $response['success'],
    		'messages'		=> $response['messages'],
    		'messages_info'	=> $response['messages_info'],
    	]);

    	if($response['success'])
    		return redirect('admin/dashboard');

    	return redirect()->back()->withErrors($response['messages'])->withInput();
    }

    public function edit($school, $id)
    {
    	$livro 				= $this->repository->find($id);
    	$authorData 		= $livro->author;

    	$livro['author'] 	= $authorData['name'];
    	$livro['author_id'] = $authorData['id'];

    	$text 				= "Editando Livro";
    	$categories 		= $this->categories;
    	unset($categories['Todos']);
    	return view('admin.livros.cadastrar-editar', compact('categories', 'livro', 'text', 'school'));
    }

    public function update(Request $request, $school, $id)
    {
    	$response = $this->service->update($request, $id);

    	session()->flash('success', [
    		'success' 		=> $response['success'],
    		'messages'		=> $response['messages'],
    		'messages_info'	=> $response['messages_info'],
    	]);

    	if($response['success'])
    		return redirect('admin/dashboard');

    	return redirect()->back()->withErrors($response['messages'])->withInput();
    }

    public function delete(Request $request, $school)
    {
        $data       = $request->all();
        $response   = $this->service->delete($data['id']);

        session()->flash('success', [
            'success'       => $response['success'],
            'messages'      => $response['messages'],
            'messages_info' => $response['messages_info'],
        ]);

        return redirect('admin/dashboard');
    }

}
