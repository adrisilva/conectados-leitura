<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\ReserveRepository;
use App\Services\AluguelService;
use App\Services\ReservaService;
use Illuminate\Http\Request;

class ReserveController extends Controller
{
	protected $service;
	protected $repository;
	protected $aluguelService;

	public function __construct(ReserveRepository $repository, ReservaService $service, AluguelService $aluguelService)
	{
		$this->middleware('auth:admin');
		$this->service 	  		= $service;
		$this->repository 		= $repository;
		$this->aluguelService	= $aluguelService;
	}
   
	public function list($school)
	{
		$reservations = $this->service->list();
		return view('admin.reservas.lista', compact('reservations', 'school'));
	}

	public function search(Request $request, $school)
	{
		$data = $request->except('_token');
		$reservations = $this->service->search($data);
		return view('admin.reservas.lista', compact('reservations', 'data', $school));
	}

	public function showRentForm($school, $id)
	{
		$data  = $this->service->getDataReserve($id);
		$dates = $this->aluguelService->geteDevolutionDates($data->category);
		return view('admin.reservas.realizar-aluguel', compact('data', 'dates', 'school'));
	}

	public function saveRent(Request $request, $school)
	{
		$data      = $request->except('_token');
		$response  = $this->service->saveRent($data);

		session()->flash('success', [
			'success'		=> $response['success'],
			'messages'  	=> $response['messages'],
			'messages_info' => $response['messages_info'],
		]);

		return redirect('admin/dashboard');
	}

	public function reserveCancel(Request $request, $school)
	{
		$data 		= $request->except('_token');
		$response 	= $this->service->reserveCancel($data);

		session()->flash('success', [
			'success'		=> true,
			'messages'  	=> $response['messages'],
			'messages_info' => $response['messages_info'],
		]);

		return redirect()->back();
	}

}
