<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use App\Services\ProfessorService;

class ProfessorController extends Controller
{

	protected $service;
	protected $repository;
	protected $matters;

    public function __construct(UserRepository $repository, ProfessorService $service)
    {
        $this->middleware('auth:admin');
        $this->repository = $repository;
        $this->service	  = $service;
        $this->matters 	  = [
	        'Algebra'			=>	'Algebra',			
	        'Artes'				=>	'Artes',			
	        'Base Técnica'		=>	'Base Técnica',	
	        'Biologia'			=>	'Biologia',	
	        'Educação Física'	=>	'Educação Física',
	        'Física'			=>	'Física',
	        'Geografia'			=>	'Geografia',
	        'Geometria'			=>	'Geometria',
			'Gramática'			=>	'Gramática',
	        'História'			=>	'História',
	        'Literatura'		=>	'Literatura',
	        'Química'			=>	'Química',
    	];
    }

    public function exibirFormularioCadastro()
    {
    	$matters = $this->matters;
    	return view('admin.professores.cadastro-editar', compact('matters'));
    }

    public function list()
    {
        $professores = $this->service->list();
        //Variável data que é usada no formulário de busca
        $data = null;
    	return view('admin.professores.listar', compact('professores', 'data'));
    }

    public function store(Request $request, $school)
    {
    	$response = $this->service->store($request->all());
    	 session()->flash('success', [
            'success'       => $response['success'],
            'messages'      => $response['messages'],
            'messages_info' => $response['messages_info'],
        ]);

        if($response['success']){
            return redirect()->route('admin.dashboard');
        }
        
        return redirect()->back()->withErrors($response['messages'])->withInput();
    }

    public function edit($school, $id)
    {
        $text = "Editando Dados";
        $professor = $this->repository->find($id);
        $matters = $this->matters;
        return view('admin.professores.cadastro-editar', compact('professor', 'text', 'matters'));
    }

    public function update(Request $request, $school, $id)
    {
        $response = $this->service->update($request->all(), $id);

        session()->flash('success', [
            'success'       => $response['success'],
            'messages'      => $response['messages'],
            'messages_info' => $response['messages_info'],
        ]);

        if($response['success'])
            return redirect()->route('professor.listar');

        return redirect()->back()->withErrors($response['messages'])->withInput();
    }

    public function delete(Request $request, $school)
    {
        $data   = $request->all();
        $id     = $data['id'];
        $response = $this->service->delete($id);

        session()->flash('success', [
            'success'       => $response['success'],
            'messages'      => $response['messages'],
            'messages_info' => $response['messages_info'],
        ]);

        return redirect()->route('professor.listar');
    }

    public function search(Request $request, $school)
    {   
        $data = $request->all();
        $professores = $this->service->search($data);
        $professores = $professores->paginate(15);
        return view('admin.professores.listar', compact('professores', 'data'));
    }

    public function searchLite(Request $request)
    {
        $data         = $request->all();
        $professores  = $this->service->search($data);
        return response()->json($professores->all());
    }
}
