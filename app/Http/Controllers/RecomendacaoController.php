<?php

namespace App\Http\Controllers;

use App\Entities\Recommendation;
use Illuminate\Http\Request;
use App\Services\RecomendacaoService;

class RecomendacaoController extends Controller
{
	protected $service;

	public function __construct(RecomendacaoService $service)
	{
		$this->middleware('auth');
		$this->service = $service;
	}

	public function recommendBook(Request $request, $school)
	{
		$data 		= $request->except('_token');
		$response	= $this->service->recommendBook($data);

		session()->flash('success', [
            'success'       => $response['success'],
            'messages'      => $response['messages'],
            'messages_info' => $response['messages_info'],
        ]);

		if($response['success'])
            return redirect('/')->withErrors($response['messages']);

        return redirect('/')->withErrors($response['messages']);
	}

	public function cancel($school, $id)
    {
        $recomendation = Recommendation::destroy($id);

        session()->flash('success', [
            'success'       => true,
            'messages'      => 'Aviso',
            'messages_info' => 'Recomendação removida com sucesso!',
        ]);

        return redirect('/');
    }
}
