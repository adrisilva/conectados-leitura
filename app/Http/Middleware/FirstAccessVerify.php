<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class FirstAccessVerify
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user() && $request->user()->first_access){

           if(Auth::guard('admin')->check()){
               return redirect('first_access_admin');
           }else{
               return redirect('first_access');
           }
        }

        return $next($request);
    }
}
