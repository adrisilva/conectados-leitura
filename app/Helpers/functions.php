<?php 

    function sendEmailPassword(array $data)
    {
        Mail::send('admin.email.template-email', $data, function($message) use ($data){
            $message->from('contato@alimentodamente.com.br', 'ADM - Conectados pela Leitura');
            $message->to($data['email']);
            $message->subject("Conectados pela Leitura");
        });
    }

    function generatePassword()
    {
        $password = null;
        for($i = 1; $i <= 6; $i++){
            $num = rand(1, 9);
            $password .= $num;
        }
        return $password;
    }
