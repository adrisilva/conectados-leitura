<?php

namespace App\Entities;

use App\Scopes\SchoolScope;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new SchoolScope);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'registration', 'birth', 'course', 'series', 'shift', 'type', 'matter', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 
    ];

    public function getSeriesAttribute()
    {
        return $this->attributes['series']."º Ano";       
    }

    public function reserve()
    {
        return $this->hasOne('App\Entities\Reserve');
    }

    public function countStudents()
    {
        return $this->where('type', 'Aluno')->count();
    }

    public function getReservedBook($user_id)
    {
//        $reserve = $this->join('reserves', 'users.id', '=', 'reserves.user_id')
//                    ->where('users.id', $user_id)
//                    ->select('reserves.id')
//                    ->get();

        $reserve = $this->hasMany(Reserve::class);
        return $reserve;
//        if(count($reserve->toArray()) > 0)
//            return $reserve[0]['id'];
    }

}
