<?php

namespace App\Entities;

use App\Scopes\SchoolScopeAdmin;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new SchoolScopeAdmin);
    }

    protected $hidden = [
        'password', 'remember_token',
    ];
}
