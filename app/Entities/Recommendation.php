<?php

namespace App\Entities;

use App\Scopes\SchoolScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Recommendation extends Model
{
    use SoftDeletes;

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new SchoolScope);
    }


    protected $fillable = ['user_id', 'book_id', 'recommendation_date', 'school_id'];

    public function book()
    {
        return $this->belongsTo(Book::class);
    }
}
