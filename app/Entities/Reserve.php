<?php

namespace App\Entities;

use App\Scopes\SchoolScope;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reserve extends Model
{
	use SoftDeletes;

    protected $dates    = ['reservation_date', 'return_date'];
	protected $fillable = ['user_id', 'book_id', 'reservation_date', 'return_date', 'status', 'school_id'];

	public function getDateNow()
	{
		return new Carbon();
	}

	public function getDateDevolutionNormal()
	{
		$dt = new Carbon();
		$dt->addDays(7);
		return $dt;
	}

	public function getDateDevolutionTechnician()
	{
		$dt = new Carbon();
		$dt->addDays(3);
		return $dt;
	}

	public function countReserves()
	{
		return $this->where('status', 'Reservado')->count();
	}

	public function countRents()
	{
		return $this->where('status', 'Alugado')->count();
	}

	public function countRentsToday()
	{
		return $this->where('return_date', $this->getDateNow()->format('Y-m-d'))
				->where('status', 'Alugado')
				->count();
	}

	public function countRentsExpired()
	{
		return $this->where('return_date', '<', $this->getDateNow()->format('Y-m-d'))
				->where('status', 'Alugado')
				->count();
	}

	public function checkBookAvailability($book_id)
	{
		return $this->where('book_id', $book_id)
					->where('status', 'Alugado')
					->orWhere('status', 'Reservado')
					->count();
	}

}

