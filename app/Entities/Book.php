<?php

namespace App\Entities;

use App\Scopes\SchoolScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Book extends Model
{
    use SoftDeletes;

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new SchoolScope);
    }
    
	protected $fillable = ['title', 'amount', 'category', 'cover', 'description', 'school_id'];

    public function author()
    {
    	return $this->hasOne('App\Entities\Author');
    }

    public function reserves()
    {
    	return $this->hasMany('App\Entities\Reserve');
    }

    public function getLeasedQuantity()
    {
    	return count($this->find($this->attributes['id'])->reserves()->where('status', 'Alugado')->get());
    }

    public function countBooks()
    {
        return $this->count();
    }

}
