<?php

namespace App\Observers;

use App\Entities\Book;
use App\Entities\School;

class BookObeserver
{
    private $school;

    public function __construct()
    {
        $school = School::where('slug', request()->school)->first();
        $this->school = $school;
    }
    /**
     * Handle to the book "created" event.
     *
     * @param  \App\Entites\Book  $book
     * @return void
     */
    public function created(Book $book)
    {
        //
    }

    /**
     * Handle the book "updated" event.
     *
     * @param  \App\Entites\Book  $book
     * @return void
     */
    public function updated(Book $book)
    {
        //
    }

    /**
     * Handle the book "deleted" event.
     *
     * @param  \App\Entites\Book  $book
     * @return void
     */
    public function deleted(Book $book)
    {
        //
    }

    public function creating(Book $book)
    {
        $book->school_id = $this->school->id;
    }
}
