<?php

namespace App\Observers;

use App\Entities\Reserve;
use App\Entities\School;

class ReserveObserver
{
    private $school;

    public function __construct()
    {
        $school = School::where('slug', request()->school)->first();
        $this->school = $school;
    }

    /**
     * Handle to the reserve "created" event.
     *
     * @param  \App\Entities\Reserve  $reserve
     * @return void
     */
    public function created(Reserve $reserve)
    {
        //
    }

    /**
     * Handle the reserve "updated" event.
     *
     * @param  \App\Entities\Reserve  $reserve
     * @return void
     */
    public function updated(Reserve $reserve)
    {
        //
    }

    /**
     * Handle the reserve "deleted" event.
     *
     * @param  \App\Entities\Reserve  $reserve
     * @return void
     */
    public function deleted(Reserve $reserve)
    {
        //
    }

    public function creating(Reserve $reserve)
    {
        $reserve->school_id = $this->school->id;
    }
}
