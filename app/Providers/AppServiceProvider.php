<?php

namespace App\Providers;

use App\Entities\Book;
use App\Entities\Reserve;
use App\Observers\BookObeserver;
use App\Observers\ReserveObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Reserve::observe(ReserveObserver::class);
        Book::observe(BookObeserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
