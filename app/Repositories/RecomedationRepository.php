<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

class RecomedationRepository extends BaseRepository {

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return "App\\Entities\\Recommendation";
    }

}