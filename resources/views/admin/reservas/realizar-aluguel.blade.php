@extends('adminlte::page')

@section('content_header')
    @include('admin.includes.notifications')
    <h1>Realizar Aluguel</h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="">Reservas</a></li>
        <li><a href="">Alugar</a></li>
    </ol>
@stop

@section('content')
    <div class="box box-primary">
   		<div class="box-body">
   			@include('admin.includes.alerts')
   			<fieldset>
				<legend>Livro</legend>
				<div class="row">
					<div class="form-group">
						{!! Form::label('title', 'Título', ['class' => 'col-sm-1']) !!}
					
						<div class="col-sm-6">
							{!! Form::text('title', $data->title, ['class' => 'form-control', 'readonly' => true]) !!}
						</div>

						{!! Form::label('category', 'Categoria ', ['class' => 'col-sm-1']) !!}

						<div class="col-sm-4">
							{!! Form::text('category', $data->category, ['class' => 'form-control', 'readonly' => true]) !!}
						</div>
					</div>
				</div>
			</fieldset>
			<p></p>
			<fieldset>
				<legend>Aluguel</legend>
				<div class="row">
					<div class="form-group">
						{!! Form::open(['url' => ['admin/reserva/concluir/reserva']]) !!}
							{!! Form::hidden('reserve_id', $data->id) !!}
							{!! Form::label('reservation_date', 'Data de Hoje', ['class' => 'col-sm-2']) !!}

							<div class="col-sm-3">
								{!! Form::date('reservation_date', $dates['now'],['class' => 'form-control', 'readonly' => true]) !!}
							</div>

							{!! Form::label('return_date', 'Data de Devolução', ['class' => 'col-sm-2']) !!}

							<div class="col-sm-3">
								{!! Form::date('return_date', $dates['devolution'], ['class' => 'form-control', 'readonly' => true]) !!}
							</div>

							<div class="col-sm-2">
								<button class="btn btn-success" id="btnConcluir"><i class="fa fa-check"></i> Concluir</button>
							</div>
						{!! Form::close() !!}
					</div>
				</div>
			</fieldset>
			<p></p>
			<fieldset>
				<legend>{{$data->type}}</legend>
				<div class="row">
					<div class="form-group">
						{!! Form::label('name', 'Nome', ['class' => 'col-sm-1']) !!}

						<div class="col-sm-3">
							{!! Form::text('name', $data->name, ['class' => 'form-control', 'disabled', 'id' => 'name']) !!}
						</div>

						{!! Form::label('course', 'Curso', ['class' => 'col-sm-1']) !!}

						<div class="col-sm-3">
							{!! Form::text('course', $data->course, ['class' => 'form-control', 'disabled', 'id' => 'course']) !!}
						</div>

						{!! Form::label('registration_field', 'Matrícula', ['class' => 'col-sm-1']) !!}
						
						<div class="col-sm-3">
							{!! Form::text('registration_field', $data->registration, ['class' => 'form-control', 'disabled', 'id' => 'registration_field']) !!}	
						</div>
					</div>
				</div>
			</fieldset>
   		</div>
   	</div>
@stop