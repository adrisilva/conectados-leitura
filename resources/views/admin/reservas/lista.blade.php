@extends('adminlte::page')

@php
    $school = request()->school;
@endphp

@section('content_header')
    @include('admin.includes.notifications')
    <h1>Reservas</h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="">Reservas</a></li>
        <li><a href="">Listar</a></li>
    </ol>
@stop

@section('content')
    <div class="box box-primary">
   		<div class="box-body">
   			@include('admin.includes.alerts')
          <div class="row">
                <div class="col-sm-12">
                    <div class="dataTables_filter">
                        {!! Form::open(['url' => ['admin/reserva/buscar'], 'method' => 'post'])!!}
                    
                        {!! Form::label('search', 'Buscar', ['class' => 'col-sm-1']) !!}

                        <div class="col-sm-4">
                            {!! Form::text('search', null,['class' => 'form-control input-sm', 'placeholder' => 'Matrícula ou Nome']) !!}
                        </div>

                        {!! Form::label('filter', 'Filtro', ['class' => 'col-sm-1']) !!}

                        <div class="col-sm-4">
                            {!! Form::select('filter', ['aluno' => 'Aluno','professor' => 'Professor','livro' => 'Livro'], 'Aluno', ['class' => 'form-control']) !!}
                        </div>
    
                        <div class="col-sm-2">
                            <button class="btn btn-primary"><i class="fa fa-search-plus"></i> Buscar</button>
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
          </div>
        <div class="dataTables_wrapper form-inline dt-bootstrap">
            <div class="row">
                <div class="col-sm-6"></div>
                <div class="col-sm-6"></div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                
                  <table class="table table-bordered table-hover dataTable" role="grid">
                      <thead>
                          <tr role="row">
                              <th>Usuário</th>
                              <th>Livro</th>
                              <th>Data de Reserva</th>                            
                              <th>Alugar</th>
                              <th>Cancelar</th>
                              <th>Detalhes</th>
                          </tr>
                      </thead>

                      <tbody>
                        @forelse($reservations as $reserve)
                          <tr role="row">
                            <th>{{$reserve->name}}</th>
                            <th>{{$reserve->title}}</th>
                            <th>{{$reserve->reservation_date->format('d/m/Y')}}</th>
                            <th><a href={{url('admin/reservas/realizar/aluguel/'.$reserve->id)}} class="btn btn-success"><i class="fa fa-exchange"></i></a></th>
                            <th><a href={{"#".$reserve->id}} class="btn btn-danger" data-confirm="devolucao" class="btn btn-success" data-izimodal-open="#modal" data-izimodal-transitionin="fadeInDown"><i class="fa fa-close"></i></a></a></th>
                            <th><a href={{url('admin/reserva/ver/detalhes/usuario/'.$reserve->user_id)}} class="btn btn-warning"><i class="fa fa-eye"></i></a></th>
                          </tr>
                        @empty
                          <tr><th>Nenhuma Reserva</th></tr>    
                        @endforelse
                      </tbody>
                  </table>
              </div>
            </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
              @if(isset($data))
                  {{$reservations->appends($data)->links()}}
              @else
                  {{$reservations->links()}}
              @endif
          </div>
        </div>
    </div>
  </div>


<link rel="stylesheet" type="text/css" href={{asset("css/iziModal/iziModal.min.css")}}>
<script src="https://code.jquery.com/jquery-3.3.1.js" crossorigin="anonymous"></script>
<script src={{asset("js/iziModal/iziModal.min.js")}}></script>

<div id="modal"  data-iziModal-icon="icon-home">
<div class="container-fluid">
  <div class="row">
    {!! Form::open(['url' => ['admin/cancelar/reserva'],'id' => 'form_cancel', 'method' => 'delete']) !!}
      {!! Form::hidden('reserve_id') !!}
      <button class="btn btn-success btn-block" id="confirm">Confirmar</button>
      <button class="btn btn-danger btn-block" data-izimodal-close="">Cancelar</button>
    {!! Form::close() !!}
  </div>
</div>
</div>

<script type="text/javascript">
  $("#modal").iziModal({
    title:'Confirmar cancelamento da reserva?',
    headerColor: '#3C8DBC',
    width: 400,
    padding: 10,
  });

    $(document).ready(function(){
      $('a[data-confirm]').click(function(ev){
        var href = $(this).attr('href');
        var form_cancel = $('#form_cancel');
          href = href.replace('#', '');
          $("[name=reserve_id]").val(href);

          $('#confirm').on('click', function(){
              form_cancel.submit();
          });
      });
    });
</script>

@stop