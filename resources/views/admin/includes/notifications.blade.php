<link rel="stylesheet" href={{asset("/css/iziToast/iziToast.min.css")}}>
<script src={{asset("/js/iziToast/iziToast.min.js")}}></script>
<script>
    iziToast.settings({
        timeout: 2000,
        resetOnHover: true,
        transitionIn: 'flipInX',
        transitionOut: 'flipOutX',
    });
</script>

@if(session('success')['success'])
    <script>
        iziToast.success({
            title: 'OK',
            message: "{{session('success')['messages']}}",
        });
    </script>
@endif

@if(isset(session('success')['success']) && !session('success')['success'] && session('success')['messages_info'] == null)
    @foreach(session('success')['messages']->all() as $error)
        <script>    
            iziToast.error({
                timeout: 3000,
                title: 'Erro',
                message: "{{$error}}",
            });
        </script>
    @endforeach   
@endif

@if(isset(session('success')['success']) && !session('success')['success'] && session('success')['messages_info'] != null)
    <script>    
        iziToast.error({
            title: 'Erro',
            message: "{{session('success')['messages']}}",
        });
    </script>
@endif

<script type="text/javascript">
    function alugarBloqueado()
    {
        iziToast.info({
            timeout: 5000,
            title: 'Esgotado',
            message: 'Não temos mais nenhum exemplar do livro solicitado no nosso acervo.',
        });
    }
</script>