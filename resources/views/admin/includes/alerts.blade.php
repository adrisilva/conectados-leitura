@if(isset($errors) && count($errors) > 0)
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{$error}}</p>
        @endforeach
    </div>
@endif

@if(session('success') && isset(session('success')['messages_info'] ))
    <div class="alert alert-success">
        <p>{{session('success')['messages_info']}}</p>
    </div>
@endif