<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript">

    $(document).ready(function(){
    $('a[data-confirm]').click(function(ev){
        var href = $(this).attr('href');
        var form_delete = $('#form_delete');
        if(!$('#confirm-delete').length){
            $('body').append(
                '<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"><div class="modal-header bg-primary text-header">EXCLUIR DADOS<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body">Tem certeza que deseja excluir esses dados?</div><div class="modal-footer"><button type="button" class="btn btn-success" data-dismiss="modal">Cancelar</button><a class="btn btn-danger text-white" id="dataComfirmOK">Apagar</a></div></div></div></div>');
        }

        href = href.replace('#', '');
        
        $("[name=id]").val(href);

        $('#confirm-delete').modal({show: true});

        $('#dataComfirmOK').on('click', function(){
            form_delete.submit();
        });

        return false;
        
    });
});

</script>