<link rel="stylesheet" type="text/css" href={{asset("css/iziModal/iziModal.min.css")}}>
<script src="https://code.jquery.com/jquery-3.3.1.js" crossorigin="anonymous"></script>
<script src={{asset("js/iziModal/iziModal.min.js")}}></script>

<div id="modal"  data-iziModal-icon="icon-home">
	<div class="container-fluid">
		<div class="row">
			{!! Form::open(['url' => ['admin/returnBook'],'id' => 'form_return']) !!}
				{!! Form::hidden('reserve_id') !!}
				<button class="btn btn-success btn-block" id="confirm">Confirmar</button>
				<button class="btn btn-danger btn-block" data-izimodal-close="">Cancelar</button>
			{!! Form::close() !!}
		</div>
	</div>
</div>

<script type="text/javascript">
	$("#modal").iziModal({
		title:'Confirmar devolução do livro?',
		headerColor: '#3C8DBC',
		width: 400,
		padding: 10,
	});

    $(document).ready(function(){
    	$('a[data-confirm]').click(function(ev){
    		var href = $(this).attr('href');
    		var form_return = $('#form_return');
	        href = href.replace('#', '');
        	$("[name=reserve_id]").val(href);

        	$('#confirm').on('click', function(){
            	form_return.submit();
        	});

		});
	});

</script>