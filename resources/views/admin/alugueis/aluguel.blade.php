@extends('adminlte::page')

@section('content_header')
    <h1>Novo Aluguel</h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="">Aluguel</a></li>
    </ol>
@stop

@section('content')
   <div class="box box-primary">
   		<div class="box-body">
   			@include('admin.includes.alerts')
			<fieldset>
				<legend>Livro</legend>
				<div class="row">
					<div class="form-group">
						{!! Form::model($livro) !!}
							{!! Form::label('title', 'Título', ['class' => 'col-sm-1']) !!}
						
							<div class="col-sm-6">
								{!! Form::text('title', null, ['class' => 'form-control', 'readonly' => true]) !!}
							</div>

							{!! Form::label('category', 'Categoria ', ['class' => 'col-sm-1']) !!}

							<div class="col-sm-4">
								{!! Form::text('category', null, ['class' => 'form-control', 'readonly' => true]) !!}
							</div>
						{!! Form::close()!!}
					</div>
				</div>
			</fieldset>
			<p></p>
			<fieldset>
				<legend>Aluguel</legend>
				<div class="row">
					<div class="form-group">
						{!! Form::open(['url' => ['admin/rent']]) !!}
							{!! Form::hidden('book_id', $livro->id) !!}
							{!! Form::hidden('registration', null, ['id' => 'user_registration'])!!}
							{!! Form::label('reservation_date', 'Data de Hoje', ['class' => 'col-sm-2']) !!}

							<div class="col-sm-3">
								{!! Form::date('reservation_date', $data['now'], ['class' => 'form-control', 'readonly' => true]) !!}
							</div>

							{!! Form::label('return_date', 'Data de Devolução', ['class' => 'col-sm-2']) !!}

							<div class="col-sm-3">
								{!! Form::date('return_date', $data['devolution'], ['class' => 'form-control', 'readonly' => true]) !!}
							</div>

							<div class="col-sm-2">
								<button class="btn btn-success" id="btnConcluir" disabled><i class="fa fa-check"></i> Concluir</button>
							</div>
						{!! Form::close() !!}
					</div>
				</div>
			</fieldset>
			<p></p>
			<fieldset>
				<legend>Aluno</legend>
				<div class="row">
					<div class="form-group">
						{!! Form::label('name', 'Nome', ['class' => 'col-sm-1']) !!}

						<div class="col-sm-3">
							{!! Form::text('name', null, ['class' => 'form-control', 'disabled', 'id' => 'name']) !!}
						</div>

						{!! Form::label('course', 'Curso', ['class' => 'col-sm-1']) !!}

						<div class="col-sm-3">
							{!! Form::text('course', null, ['class' => 'form-control', 'disabled', 'id' => 'course']) !!}
						</div>

						{!! Form::label('registration_field', 'Matrícula', ['class' => 'col-sm-1']) !!}
						
						<div class="col-sm-3">
							{!! Form::text('registration_field', null, ['class' => 'form-control', 'disabled', 'id' => 'registration_field']) !!}	
						</div>
					</div>
				</div>
			</fieldset>
			<p></p>
			<fieldset>
				<legend>Buscar</legend>
				<div class="row">
					<div class="col-sm-12">
	            		<div class="dataTables_filter">
  
			                {!! Form::label('search', 'Buscar', ['class' => 'col-sm-1']) !!}

			                <div class="col-sm-4">
			                    {!! Form::text('search', null,['class' => 'form-control input-sm', 'placeholder' => 'Matrícula ou Nome', 'id' => 'search']) !!}
			                </div>
							<div class="col-sm-4">
			                	{!! Form::select('searchType', ['Aluno', 'Professor'], null, ['class' => 'form-control', 'id' => 'searchType']) !!}
							</div>
							<div class="col-sm-2">
			                    <button class="btn btn-primary" id="btnSearch"><i class="fa fa-search-plus"></i> Buscar</button>
			                </div>
	            		</div>
        			</div>
				</div>

				<div class="dataTables_wrapper form-inline dt-bootstrap">
            		<div class="row">
                		<div class="col-sm-6"></div>
           		 		<div class="col-sm-6"></div>
            		</div>
            		<div class="row">
                		<div class="col-sm-12">
                    		<table class="table table-bordered table-hover dataTable" role="grid">
		                        <thead>
		                            <tr role="row">
		                                <th>Matrícula</th>
		                                <th>Nome</th>                         
		                                <th>Curso</th>
		                                <th>Série</th>
		                                <th>Selecionar</th>
		                            </tr>
                        		</thead>

                        		<tbody  id="tbody"></tbody>
                        	</table>
                        </div>
			</fieldset>
   		</div>
   	</div>

	<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
	<script>
		$('#btnSearch').click(function(){
			var searchType  = $('#searchType').val();
			var search 		= $('#search').val();
			var dataForm  	= 'search='+search;
			var url; 	

			if(searchType == 0)
				url = "{{ url('admin/alugar/buscar/aluno?_data_') }}".replace('_data_', dataForm);
			else
				url = "{{ url('admin/professor/buscar/professor?_data_') }}".replace('_data_', dataForm);

			$.get(url, function(data){
				$("#tbody").empty();
				var count = Object.keys(data).length;
				if(count == 0){
					$("#tbody").append('<tr><th colspan=5>Nenhum Usuário Encontrado<th></tr>');
				}else{
					$.each(data, function(key, value){
						var nome  = value.name;
						var curso = (value.course == null) ? "-" : value.course;
						var serie = (value.series == "º Ano") ? "-" :value.series;
						$("#tbody").append(
							'<tr><td>'+value.registration+'</td>'+
							'<td>'+nome+'</td>'+
							'<td>'+curso+'</td>'+
							'<td>'+serie+'</td>'+
							'<td><button class="btn-link selecionar">Selecionar</button></td></tr>'
						);
					});
				}
				setDataAluno();
			});
		});	

		function setDataAluno()
		{
			$('.selecionar').click(function(){
				var matricula = $(this).parents('tr').find('td').eq(0).text();
				var name = $(this).parents('tr').find('td').eq(1).text();
				var curso = $(this).parents('tr').find('td').eq(2).text();
				$('#name').val(name);
				$('#course').val(curso);
				$('#registration_field').val(matricula);
				$('#user_registration').val(matricula);
				$('#btnConcluir').prop("disabled", false);
			});
		}
	</script>
@stop