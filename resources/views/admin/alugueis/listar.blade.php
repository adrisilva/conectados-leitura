@extends('adminlte::page')

@php
    $school = request()->school;
@endphp

@section('content_header')
    @include("admin.includes.return-modal")
    @include('admin.includes.notifications')
    <h1>Alugueis</h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="">Alugueis</a></li>
        <li><a href="">Listar</a></li>
    </ol>
@stop

@section('content')
   <div class="box box-primary">
   		<div class="box-body">
   			{{-- @include('admin.includes.alerts') --}}
            <div class="row">
                <div class="col-sm-12">
                    <div class="dataTables_filter">
                        {!! Form::open(['url' => ['admin/alugueis/buscar'], 'method' => 'post'])!!}
                    
                        {!! Form::label('search', 'Buscar', ['class' => 'col-sm-1']) !!}

                        <div class="col-sm-4">
                            {!! Form::text('search', null,['class' => 'form-control input-sm', 'placeholder' => 'Matrícula ou Nome']) !!}
                        </div>

                        {!! Form::label('filter', 'Filtro', ['class' => 'col-sm-1']) !!}

                        <div class="col-sm-4">
                            {!! Form::select('filter', ['aluno' => 'Aluno','professor' => 'Professor','livro' => 'Livro'], 'Aluno', ['class' => 'form-control']) !!}
                        </div>
    
                        <div class="col-sm-2">
                            <button class="btn btn-primary"><i class="fa fa-search-plus"></i> Buscar</button>
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
			
			<div class="dataTables_wrapper form-inline dt-bootstrap">
            <div class="row">
                <div class="col-sm-6"></div>
                <div class="col-sm-6"></div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                
                    <table class="table table-bordered table-hover dataTable" role="grid">
                        <thead>
                            <tr role="row">
                                <th>Usuário</th>
                                <th>Livro</th>
                                <th>Data de Reserva</th>                            
                                <th>Data de Devolução</th>
                                <th>Renovar</th>
                                <th>Devolver</th>
                                <th>Detalhes</th>
                            </tr>
                        </thead>

                        <tbody>
                        	@forelse($rents as $rent)
								<tr>
									<th>{{$rent->name}}</th>
									<th>{{$rent->title}}</th>
									<th>{{$rent->reservation_date->format('d/m/Y')}}</th>
									<th>{{$rent->return_date->format('d/m/Y')}}</th>
									<th>
										{!! Form::open(['url' => ['admin/renovar/aluguel/'.$rent->id], 'method' => 'get']) !!}
											<button class="btn btn-primary"><i class="fa fa-calendar"></i></button>
										{!! Form::close() !!}
									</th>
									<th>
										<a href="#{{$rent->id}}" data-confirm="devolucao" class="btn btn-success" data-izimodal-open="#modal" data-izimodal-transitionin="fadeInDown"><i class="fa fa-undo"></i></a>
									</th>
                                    <th>
                                        <a href={{url('admin/aluguel/ver/detalhes/usuario/'.$rent->user_id)}} class="btn btn-warning"><i class="fa fa-eye"></i></a>
                                    </th>
								</tr>
                        	@empty
								<tr><th colspan="6">Nenhum Aluguel</th></tr>
                        	@endforelse

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
               <div class="col-sm-12">
                    @if(isset($data))
                        {{$rents->appends($data)->links()}}
                    @else
                        {{$rents->links()}}
                    @endif
               </div>
            </div>
   		</div>
   	</div>
@stop