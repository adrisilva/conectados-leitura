@extends('adminlte::page')

@section('content_header')
    <h1>{{$user->type}}</h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="">Usuário</a></li>
        <li><a href="">Dados</a></li>
    </ol>
@stop

@section('content')
    {!! Form::model($user) !!}
    <div class="box box-primary">
   		<div class="box-body">
         <div class="row">
            <div class="form-group">
                {!! Form::label('name', 'Nome', ['class' => 'col-sm-2']) !!}
                    
                <div class="col-sm-10">
                    {!! Form::text('name', null,['class' => 'form-control', 'placeholder' => 'Digite o primeiro nome do aluno', 'disabled']) !!}
                </div>
            </div>
        </div>

        <p></p>
        
        <div class="row">
            <div class="form-group">
                {!! Form::label('birth', 'Data de Nascimento', ['class' => 'col-sm-2']) !!}
                    
                <div class="col-sm-4">
                    {!! Form::date('birth', null,['class' => 'form-control', 'disabled']) !!}
                </div>
    
                {!! Form::label('registration', 'Matrícula', ['class' => 'col-sm-2', 'max' => '7', 'min' => '7']) !!}
                    
                <div class="col-sm-4">
                    {!! Form::text('registration', null,['class' => 'form-control', 'placeholder' => 'Digite a matrícula do aluno', 'disabled']) !!}
                </div>
            </div>
        </div>

        <p></p>

        <div class="row">
            <div class="form-group">
                {!! Form::label('email', 'E-Mail', ['class' => 'col-sm-2']) !!}
                    
                <div class="col-sm-10">
                    {!! Form::email('email', null,['class' => 'form-control', 'placeholder' => 'Digite o e-mail do aluno', 'disabled']) !!}
                </div>
            </div>
        </div>

        <p></p>

        <div class="row">
            <div class="form-group">
                {!! Form::label('shift', 'Turno', ['class' => 'col-sm-2']) !!}
                    
                <div class="col-sm-10">
                    {!! Form::text('shift', null, ['class' => 'form-control', 'placeholder' => 'Selecione o turno do aluno', 'disabled']) !!}
                </div>
            </div>
        </div>

        <p></p>

        @if($user->type == "Aluno")
          <div class="row">
            <div class="form-group">
                {!! Form::label('course', 'Curso', ['class' => 'col-sm-2']) !!}
                    
                <div class="col-sm-4">
                    {!! Form::text('course', null, ['class' => 'form-control', 'placeholder' => 'Selecione o curso do aluno', 'id' => 'course', 'disabled']); !!}
                </div>
    
                {!! Form::label('series', 'Série', ['class' => 'col-sm-2']) !!}
                <div class="col-sm-4">
                    {!! Form::text('series', null, ['class' => 'form-control', 'placeholder' => 'Selecione a série do aluno', 'id' => 'series', 'disabled']); !!}
                </div>
            </div>
          </div>
        @endif

        <p></p>
        
        <div class="row">
          <div class="col-sm-12">
            @if($origin == "aluguel")
              <a href={{url("admin/alugueis")}} class="btn btn-primary btn-block">Voltar</a>
            @else
              <a href={{url("admin/reservas")}} class="btn btn-primary btn-block">Voltar</a>
            @endif
          </div>
        </div>

      </div>
    </div>
@stop