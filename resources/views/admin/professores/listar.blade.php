
@extends('adminlte::page')

@section('title', 'Painel Administrativo')

@section('content_header')
    @include('admin.includes.notifications')
    <h1>Professores</h1>
    {{--<br>--}}
    {{--<a class="btn btn-social btn-success" href="{!! route('professor.cadastro') !!}">--}}
        {{--<i class="fa fa-plus"></i> Cadastrar Professor--}}
    {{--</a>--}}
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="">Professores</a></li>
        <li><a href="">Listar</a></li>
    </ol>
@stop

@section('content')
<div class="box box-primary">
    <div class="box-body">
        <div class="col-sm-12">
            <div class="dataTables_filter">
                {!! Form::model($data, ['url' => ['admin/professores/buscar'], 'method' => 'get']) !!}
               
                {!! Form::label('search', 'Buscar', ['class' => 'col-sm-1']) !!}

                <div class="col-sm-4">
                    {!! Form::text('search', null,['class' => 'form-control input-sm', 'placeholder' => 'Nome do Professor']) !!}
                </div>

                <div class="col-sm-2">
                    <button class="btn btn-primary"><i class="fa fa-search"></i> Procurar</button>
                </div>

                {!! Form::close() !!}
            </div>
        </div>

        <div class="dataTables_wrapper form-inline dt-bootstrap">
            <div class="row">
                <div class="col-sm-6"></div>
                <div class="col-sm-6"></div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                
                    <table class="table table-bordered table-hover dataTable" role="grid">
                        <thead>
                            <tr role="row">
                                <th>CPF</th>
                                <th>Nome</th>
                                <th>E-Mail</th>                            
                                <th>Matéria</th>
                                {{--<th>Editar</th>--}}
                                {{--<th>Excluir</th>--}}
                            </tr>
                        </thead>
    
                        <tbody>
                            
                             @forelse ($professores as $professor)
                                <tr>
                                    <td>{{$professor->cpf}}</td>
                                    <td>{{$professor->name}}</td>
                                    <td>{{$professor->email}}</td>
                                    <td>{{$professor->matter}}</td>
                                    {{--<td><a href={{route('professor.edit', $professor->id)}} class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>--}}

                                    {{--<td>--}}
                                        {{--<a href="#{{$professor->id}}" class="btn btn-danger" data-confirm='Tem certeza que deseja excluir os dados desse professor?'>--}}
                                            {{--<i class="fa fa-trash"></i>--}}
                                        {{--</a>--}}
                                    {{--</td>--}}
                                </tr>
                            @empty
                                <tr>
                                    <th colspan="7">Nenhum Professor Encontrado</th>
                                </tr>
                            @endforelse

                        </tbody>
                    </table>
                </div>
                
            </div>
            
            <div class="row">

                <div class="col-sm-5"></div>
                   
                <div class="col-sm-7">
                    {!! $professores->links() !!}
                </div>

            </div>
        </div>
    </div>
</div>

{{--{!! Form::open(['route' => 'professor.delete', 'method' => 'delete', 'id' => 'form_delete']) !!}--}}
    {{--{!! Form::hidden('id') !!}--}}
{{--{!! Form::close() !!}--}}

@include('admin.includes.modals')

@stop