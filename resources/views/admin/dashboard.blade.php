@extends('adminlte::page')

@section('title', 'Painel Administrativo')

@section('content_header')
    @include('admin.includes.notifications')
    <h1>Painel de Controle</h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i>Home</a></li>
    </ol>
@stop

@section('content')

<div class="box">
    <div class="box-body">
    	<div class="row">
    		<div class="col-sm-12">
    			@include('admin.includes.alerts')
    		</div>
    	</div>
		<div class="row">
	    	<div class="col-lg-3 col-xs-6">
	          <div class="small-box bg-yellow">
	            <div class="inner">
	              <h3>{{$students}}</h3>
	              <p>Alunos Cadastrados</p>
	            </div>
	            <div class="icon">
	              <i class="ion ion-person-add"></i>
	            </div>
	            <a href={{url("admin/alunos")}} class="small-box-footer">Mais Informações <i class="fa fa-arrow-circle-right"></i></a>
	          </div>
	        </div>

	        <div class="col-lg-3 col-xs-6">
		      	<div class="small-box bg-green">
		            <div class="inner">
		              <h3>{{$books}}</h3>
		              <p>Livros Cadastrados</p>
		            </div>
		        	<div class="icon">
		        	  <i class="ion ion-ios-book"></i>
		        	</div>
		            <a href={{url("admin/livros")}} class="small-box-footer">Mais Informações <i class="fa fa-arrow-circle-right"></i></a>
		  		</div>
	    	</div>

	    	<div class="col-lg-3 col-xs-6">
		      	<div class="small-box bg-aqua">
		            <div class="inner">
		              <h3>{{$reserves}}</h3>
		              <p>Livros Reservados</p>
		            </div>
		        	<div class="icon">
		        	  <i class="ion ion-ios-bookmarks"></i>
		        	</div>
		            <a href={{url("admin/reservas")}} class="small-box-footer">Mais Informações <i class="fa fa-arrow-circle-right"></i></a>
		  		</div>
	    	</div>

	    	<div class="col-lg-3 col-xs-6">
		      	<div class="small-box bg-blue">
		            <div class="inner">
		              <h3>{{$rents}}</h3>
		              <p>Livros Alugados</p>
		            </div>
		        	<div class="icon">
		        	  <i class="ion ion-clipboard"></i>
		        	</div>
		            <a href={{url("admin/alugueis")}} class="small-box-footer">Mais Informações <i class="fa fa-arrow-circle-right"></i></a>
		  		</div>
	    	</div>
		</div>

		 <div class="row">

	        <div class="col-lg-3 col-xs-6">
		      	<div class="small-box bg-purple">
		            <div class="inner">
		              <h3>{{$rentsToday}}</h3>
		              <p>Alugueis para Hoje</p>
		            </div>
		        	<div class="icon">
		        	  <i class="ion ion-calendar"></i>
		        	</div>
		            <a href={{url("admin/alugueis")}} class="small-box-footer">Mais Informações <i class="fa fa-arrow-circle-right"></i></a>
		  		</div>
	    	</div>
	    	
	    	<div class="col-lg-3 col-xs-6">
		      	<div class="small-box bg-red">
		            <div class="inner">
		              <h3>{{$rentsExpired}}</h3>
		              <p>Alugueis expirados</p>
		            </div>
		        	<div class="icon">
		        	  <i class="ion ion-alert"></i>
		        	</div>
		            <a href={{url("admin/alugueis")}} class="small-box-footer">Mais Informações <i class="fa fa-arrow-circle-right"></i></a>
		  		</div>
	    	</div>

		</div>
	</div>
</div>
@stop