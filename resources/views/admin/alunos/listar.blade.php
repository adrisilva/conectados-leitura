@extends('adminlte::page')

@section('title', 'Painel Administrativo')

@section('content_header')
@include('admin.includes.notifications')
<h1>Alunos</h1>

{{--<br>--}}
{{--<a class="btn btn-social btn-success" href="{!! route('aluno.cadastro') !!}">--}}
    {{--<i class="fa fa-plus"></i> Cadastrar Aluno--}}
{{--</a>--}}

<ol class="breadcrumb">
    <li><a href=""><i class="fa fa-dashboard"></i>Home</a></li>
    <li><a href="">Alunos</a></li>
    <li><a href="">Listar</a></li>
</ol>
@stop

@section('content')
<div class="box box-primary">
    <div class="box-body">
        <div class="col-sm-12">
            <div class="dataTables_filter">
                {!! Form::model($data, ['url' => ['admin/alunos/buscar'], 'method' => 'get']) !!}
                
                {!! Form::label('search', 'Buscar', ['class' => 'col-sm-1']) !!}

                <div class="col-sm-4">
                    {!! Form::text('search', null,['class' => 'form-control input-sm', 'placeholder' => 'Matrícula ou Nome do Aluno']) !!}
                </div>

                {!! Form::label('course', 'Curso', ['class' => 'col-sm-1']) !!}

                <div class="col-sm-4">
                 {!! Form::select('course', $courses, null, ['class' => 'form-control', 'placeholder' => 'Selecione o curso do aluno']); !!}
             </div>

             <div class="col-sm-2">
                <button class="btn btn-primary"><i class="fa fa-search-plus"></i> Buscar</button>
            </div>

            {!! Form::close() !!}
        </div>
    </div>

    <div class="dataTables_wrapper form-inline dt-bootstrap">
        <div class="row">
            <div class="col-sm-6"></div>
            <div class="col-sm-6"></div>
        </div>  
        <div class="table-responsive">

            <table class="table table-bordered table-hover dataTable" role="grid">
                <thead>
                    <tr role="row">
                        <th>Matrícula</th>
                        <th>Nome</th>
                        <th>E-Mail</th>                            
                        <th>Curso</th>
                        <th>Série</th>
                        {{--<th>Editar</th>--}}
                        {{--<th>Excluir</th>--}}
                    </tr>
                </thead>
                
                <tbody>
                    
                    @forelse ($alunos as $aluno)
                    <tr>
                        <td>{{$aluno->registration}}</td>
                        <td>{{$aluno->name}}</td>
                        <td>{{$aluno->email}}</td>
                        <td>{{$aluno->course}}</td>
                        <td>{{$aluno->series}}</td>
                        {{--<td><a href={{route('aluno.edit', $aluno->id)}} class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>--}}
                        {{--<td>--}}
                            {{--<a href="#{{$aluno->id}}" class="btn btn-danger" data-confirm='Tem certeza que deseja excluir os dados desse aluno?'>--}}
                                {{--<i class="fa fa-trash"></i>--}}
                            {{--</a>--}}
                        {{--</td>--}}
                    </tr>
                    @empty
                    <tr>
                        <th colspan="7">Nenhum Aluno Encontrado</th>
                    </tr>
                    @endforelse

                </tbody>
            </table>
            
        </div>
        
        <div class="row">                   
            <div class="col-sm-12">
                
                @if(isset($data))
                {!! $alunos->appends($data)->links() !!}
                @else
                {!! $alunos->links() !!}
                @endif
            </div>

        </div>
    </div>
</div>
</div>

{{--{!! Form::open(['route' => 'aluno.delete', 'method' => 'delete', 'id' => 'form_delete']) !!}--}}
{{--{!! Form::hidden('id') !!}--}}
{{--{!! Form::close() !!}--}}

{{--@include('admin.includes.modals')--}}

@stop