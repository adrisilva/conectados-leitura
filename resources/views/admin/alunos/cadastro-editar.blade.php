@extends('adminlte::page')

@section('content_header')

    <h1>{{$text or 'Cadastro Aluno'}}</h1>

    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="">Alunos</a></li>
        <li><a href="">Cadastro</a></li>
    </ol>
@stop

@section('content')
   
@if(isset($aluno))
    {!! Form::model($aluno, ['route' => ['aluno.update', $aluno->id], 'method' => 'put']) !!}
@else
    {!! Form::open(['route' => 'aluno.cadastro']) !!}
@endif

<div class="box box-primary">
    
    <div class="box-body">
        
        @include('admin.includes.alerts')

        <div class="row">
            <div class="form-group">
                {!! Form::label('name', 'Nome', ['class' => 'col-sm-2']) !!}
                    
                <div class="col-sm-10">
                    {!! Form::text('name', null,['class' => 'form-control', 'placeholder' => 'Digite o primeiro nome do aluno']) !!}
                </div>
            </div>
        </div>

        <p></p>
        
        <div class="row">
            <div class="form-group">
                {!! Form::label('birth', 'Data de Nascimento', ['class' => 'col-sm-2']) !!}
                    
                <div class="col-sm-4">
                    {!! Form::date('birth', null,['class' => 'form-control']) !!}
                </div>
    
                {!! Form::label('registration', 'Matrícula', ['class' => 'col-sm-2', 'max' => '7', 'min' => '7']) !!}
                    
                <div class="col-sm-4">
                    {!! Form::text('registration', null,['class' => 'form-control', 'placeholder' => 'Digite a matrícula do aluno']) !!}
                </div>
            </div>
        </div>

        <p></p>

        <div class="row">
            <div class="form-group">
                {!! Form::label('email', 'E-Mail', ['class' => 'col-sm-2']) !!}
                    
                <div class="col-sm-10">
                    {!! Form::email('email', null,['class' => 'form-control', 'placeholder' => 'Digite o e-mail do aluno']) !!}
                </div>
            </div>
        </div>

        <p></p>

        <div class="row">
            

            <div class="form-group">
                <label class="col-sm-2">
                    Escola    
                </label>

                <div class="col-sm-10">
                    <select class="form-control" onclick="desativarCurso()" id="school_type" name="school_type">
                        <option value="eeep">Escola Estadual de Educação Profissional - EEEP</option>
                        <option value="integral">Escola de Tempo Integral</option>
                        <option value="regular">Escola Regular</option>
                    </select>
                </div>
            </div>
        </div>

        <p></p>

        <div class="row">
            <div class="form-group">
                {!! Form::label('shift', 'Turno', ['class' => 'col-sm-2']) !!}
                    
                <div class="col-sm-10">
                    {!! Form::select('shift', ['Integral' => 'Integral', 'Manhã' => 'Manhã', 'Tarde' => 'Tarde', 'Noite' => 'Noite'],'Integral',['class' => 'form-control', 'placeholder' => 'Selecione o turno do aluno']) !!}
                </div>
            </div>
        </div>

        <p></p>

        <div class="row">
            <div class="form-group">
                {!! Form::label('course', 'Curso', ['class' => 'col-sm-2']) !!}
                    
                <div class="col-sm-4">
                    {!! Form::select('course', $courses, null, ['class' => 'form-control', 'placeholder' => 'Selecione o curso do aluno', 'id' => 'course']); !!}
                </div>
    
                {!! Form::label('series', 'Série', ['class' => 'col-sm-2']) !!}
                <div class="col-sm-4">
                    {!! Form::select('series', $series, '1',['class' => 'form-control', 'placeholder' => 'Selecione a série do aluno', 'id' => 'series']); !!}
                </div>
            </div>
        </div>

        <p></p>

        @if(isset($aluno))
            <div class="row">
                <div class="form-group">
                    {!! Form::label('password', 'Senha', ['class' => 'col-sm-2']) !!}
                        
                    <div class="col-sm-10">
                        {!! Form::password('password',['class' => 'form-control', 'placeholder' => 'Digite a nova senha de acesso']); !!}
                    </div>
                </div>
            </div>
        @endif
        <p></p>

        <div class="row">
            <div class="form-group">
                <div class="col-sm-2">
                    <button type="button" class="btn btn-primary" onclick="history.go(-1)">Voltar</button>
                </div>
                <div class="col-sm-10">
                    {!! Form::submit('Salvar', ['class' => 'btn btn-block btn-success']) !!}
                </div>
            </div>
        </div>

    </div>
</div>
   
{!! Form::close() !!}


<script type="text/javascript">
    
    function desativarCurso()
    {
        var school_type = document.getElementById('school_type').value;

        var course = document.getElementById('course');

        if(school_type != 'eeep'){
            course.disabled = true;
            mudarSeries();
        }else{
            course.disabled = false;
            mudarSeries('eeep');
        }
    }

    function mudarSeries(type = null)
    {
        var series = document.getElementById('series');
        var opt = series.getElementsByTagName("option");

        if(type == 'eeep'){
            opt[4].selected = 'selected';
        }else{
            opt[1].selected = 'selected';
        }
    }

</script>

@stop