@extends('adminlte::page')
@php
    $school = request()->school;
@endphp
@section('title', 'Painel Administrativo')

@section('content_header')
    @include('admin.includes.notifications')
    <h1>Livros</h1>
    <br>
    <a class="btn btn-social btn-success" href="{!! url('admin/cadastro/livro') !!}">
        <i class="fa fa-plus"></i> Cadastrar Livro
    </a>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="">Livros</a></li>
        <li><a href="">Listar</a></li>
    </ol>
@stop

@section('content')
<div class="box box-primary">
    <div class="box-body">
        <div class="col-sm-12">
            <div class="dataTables_filter">
                {!! Form::model($data, ['url' => ['admin/livros-search']]) !!}
               
                {!! Form::label('search', 'Buscar', ['class' => 'col-sm-1']) !!}

                <div class="col-sm-2">
                    {!! Form::text('search', null, ['class' => 'form-control input-sm', 'placeholder' => 'Busca']) !!}
                </div>

                {!! Form::label('filter', 'Filtro', ['class' => 'col-sm-1']) !!}

                <div class="col-sm-2">
                    {!! Form::select('filter',['book_title' => 'Título do Livro', 'author_name' => 'Nome do Autor'] ,null, ['class' => 'form-control input-sm']) !!}
                </div>

                {!! Form::label('category', 'Categoria', ['class' => 'col-sm-1']) !!}

                <div class="col-sm-3">
                   {!! Form::select('category', $categories , null, ['class' => 'form-control']); !!}
                </div>

                <div class="col-sm-2">
                    <button class="btn btn-primary"><i class="fa fa-search"></i> Buscar</button>
                </div>

                {!! Form::close() !!}
            </div>
        </div>

        <div class="dataTables_wrapper form-inline dt-bootstrap">
            <div class="row">
                <div class="col-sm-6"></div>
                <div class="col-sm-6"></div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                
                    <table class="table table-bordered table-hover dataTable" role="grid">
                        <thead>
                            <tr role="row">
                                <th>Título</th>
                                <th>Categoria</th>
                                <th>Quantidade Total</th>      
                                <th>Quantidade Alugada</th>
                                <th>Autor</th>
                                <th>Alugar</th>
                                <th>Editar</th>
                                <th>Excluir</th>
                            </tr>
                        </thead>
    
                        <tbody>
                            
                            @forelse ($livros as $livro)
                                <tr>
                                    <td>{{$livro->title}}</td>
                                    <td>{{$livro->category}}</td>
                                    <td>{{$livro->amount}}</td>
                                    <td>{{$livro->getLeasedQuantity()}}</td>
                                    <td>{{$livro->name}}</td>
                                    <td>
                                        @if($livro->getLeasedQuantity() == $livro->amount)
                                            <a href="#" disabled class="btn btn-success" title="Alugar" onclick="alugarBloqueado()"><i class="fa fa-exchange"></i></a>
                                        @else
                                            <a href="{{url('admin/alugar/livro/'.$livro->id)}}" class="btn btn-success" title="Alugar"><i class="fa fa-exchange"></i></a>
                                        @endif
        

                                    </td>
                                    <td><a href="{{url('admin/editar/livro/'.$livro->id)}}" class="btn btn-primary" title="Editar"><i class="fa fa-pencil"></i></a></td>

                                    <td>
                                        <a href="#{{$livro->id}}" class="btn btn-danger" data-confirm='Tem certeza que deseja excluir os dados desse livro?' title="Excluir">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <th colspan="7">Nenhum livro Encontrado</th>
                                </tr>
                            @endforelse

                        </tbody>
                    </table>
                </div>
                
            </div>
            
            <div class="row">
                   
                <div class="col-sm-12">
                    @if(isset($data))
                        {!! $livros->appends($data)->links() !!}
                    @else
                        {!! $livros->links() !!}
                    @endif
                </div>

            </div>
        </div>
    </div>
</div>

{!! Form::open(['url' => ['admin/delete/livro'], 'method' => 'delete', 'id' => 'form_delete']) !!}
    {!! Form::hidden('id') !!}
{!! Form::close() !!}

@include('admin.includes.modals')

@stop