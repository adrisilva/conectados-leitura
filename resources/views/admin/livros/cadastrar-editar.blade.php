@extends('adminlte::page')
@php
	$school = request()->school;
@endphp
@section('content_header')

    <h1>{{$text or 'Cadastro Livro'}}</h1>

    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="">Livros</a></li>
        <li><a href="">Cadastro</a></li>
    </ol>
@stop

@section('content')
   <div class="box box-primary">
   		<div class="box-body">

			@include('admin.includes.alerts')
			
				@if(isset($livro))
					{!! Form::model($livro, ['url' => [ 'admin/editar/livro/'.$livro->id], 'method' => 'put', 'enctype' => 'multipart/form-data']) !!}
					{!! Form::hidden('author_id', $livro['author_id']) !!}
				@else
					{!! Form::open(['url' => ['admin/cadastro/livro'], 'enctype' => 'multipart/form-data']) !!}
				@endif

				<div class="row">
					<div class="form-group">
						{!! Form::label('title', 'Título', ['class' => 'col-sm-2']) !!}

						<div class="col-sm-10">
							{!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Título do Livro']) !!}
						</div>
					</div>
				</div>

				<p></p>

				<div class="row">
					<div class="form-group">
						{!! Form::label('category', 'Categoria', ['class' => 'col-sm-2']) !!}
						
						<div class="col-sm-6">
							{!! Form::select('category', $categories, null, ['class' => 'form-control', 'placeholder' => 'Selecione a Categoria do Livro']) !!}
						</div>

						{!! Form::label('amount', 'Quantidade', ['class' => 'col-sm-1']) !!}
						<div class="col-sm-3">
							{!! Form::number('amount', null, ['class' => 'form-control', 'placeholder' => 'Quantidade total de livros', 'min' => '1']) !!}
						</div>
					</div>
				</div>

				<p></p>

				<div class="row">
					<div class="form-group">
						{!! Form::label('author', 'Autor', ['class' => 'col-sm-2']) !!}

						<div class="col-sm-10">
							{!! Form::text('author', null, ['class' => 'form-control', 'placeholder' => 'Nome do Autor']) !!}
						</div>
					</div>
				</div>

				<p></p>
				
				<div class="row">
					<div class="form-group">
						{!! Form::label('description', 'Descrição', ['class' => 'col-sm-2']) !!}

						<div class="col-sm-10">
							{!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Descrição do livro']) !!}
						</div>
					</div>
				</div>		
			
				<p></p>

				<div class="form-group">
					{!! Form::label('cover', 'Capa', ['class' => 'col-sm-2']) !!}
					<input type="file" class="dropify" data-default-file="{{ isset($livro) ? asset('covers/'.$livro->cover) : '' }}"
                      name="cover" id="cover" data-show-loader="true" accept="image/*">
				</div>

				<div class="row">	
					<div class="form-group">
						
						<div class="col-sm-4">
							{!! Form::submit('Salvar', ['class' => 'btn btn-success btn-block']) !!}
						</div>
						
						<div class="col-sm-2">
                    		<button type="button" class="btn btn-primary" onclick="history.go(-1)">Voltar</button>
                		</div>
					</div>
				</div>

			{!! Form::close() !!}
   		</div>
   </div>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"></script>
   	
   	<script type="text/javascript">
		$('.dropify').dropify();
				
   	</script>
@stop