@extends('site.layouts.app')

@prepend('load-class-home')
  <script>
    document.getElementById("body").className = "home"; 
    document.getElementById("btnLogin").className = "btn navbar-btn btn-white";
  </script>
@endprepend

@section('content')

  <div id="carousel-home" data-ride="carousel" class="carousel slide carousel-fullscreen carousel-fade">
    <!-- Indicators-->
    <ol class="carousel-indicators">
    <li data-target="#carousel-home" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-home" data-slide-to="1"></li>
    <li data-target="#carousel-home" data-slide-to="2"></li>
    </ol>
    <!-- Wrapper for slides-->
    <div role="listbox" class="carousel-inner">
      <div style="background-image: url('images/carousel3.jpg');" class="item active">
        <div class="overlay"></div>
        <div class="carousel-caption">
          <h1 class="super-heading">Alimento da Mente</h1>
          <p class="super-paragraph">Escola Estadual de Educação Profissional Professor Onélio Porto</a></p>
        </div>
      </div>
      <div style="background-image: url('images/carousel2.jpg');" class="item">
        <div class="overlay"></div>
        <div class="carousel-caption">
          <h1 class="super-heading">Web sistema bibliotecário</h1>
          <p class="super-paragraph">Desenvolvido pela turma de Informática 2016-2018</p>
        </div>
      </div>
      <div style="background-image: url('images/carousel1.jpg');" class="item">
        <div class="overlay"></div>
        <div class="carousel-caption">
          <h1 class="super-heading">Leia um livro hoje</h1>
          <p class="super-paragraph">"O homem que não lê bons livros não tem nenhuma vantagem sobre o homem que não sabe ler."</p>
        </div>
      </div>
    </div>
  </div>
  <section class="background-gray-lightest">
    <div class="container">
      <h1> O que é o projeto Alimento da Mente?</h1>
      <p class="lead">&ensp;O Alimento da Mente(ADM) é o web sistema bibliotecário para consulta de livros técnicos e de base regular da EEEP Professor Onélio Porto, feito para auxiliar na vida acadêmica de todos os alunos e professores da escola.<br>&ensp;O sistema conta com um avançado sistema de pesquisa com todos os livros disponíveis na biblioteca da escola, desenvolvido pela turma do curso de Informática 2016-2018.</p>
      <p> <a href="text.php" class="btn btn-ghost">Saiba mais   </a></p>
    </div>
  </section>
  <!-- portfolio-->
  <section id="portfolio" class="section--no-padding-bottom">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          <h1>Galeria ou portifólio</h1>
          <p class="lead margin-bottom--big">Crie aqui uma galeria de fotos ou um portifólio do site</p>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row no-space">
        <div class="col-lg-3 col-sm-4 col-xs-6">
          <div class="box"><a href="images/portfolio-1.jpg" title="" data-lightbox="portfolio" data-title="Portfolio image 1"><images src="images/portfolio-1.jpg" alt="" class="images-responsive"></a></div>
        </div>
        <div class="col-lg-3 col-sm-4 col-xs-6">
          <div class="box"><a href="images/portfolio-2.jpg" title="" data-lightbox="portfolio" data-title="Portfolio image 2"><images src="images/portfolio-2.jpg" alt="" class="images-responsive"></a></div>
        </div>
        <div class="col-lg-3 col-sm-4 col-xs-6">
          <div class="box"><a href="images/glr-3.jpeg" title="" data-lightbox="portfolio" data-title="Turma Informática 2016-2018 completa em 2017"><images src="images/glr-3.jpeg" alt="" class="images-responsive"></a></div>
        </div>
        <div class="col-lg-3 col-sm-4 col-xs-6">
          <div class="box"><a href="images/portfolio-4.jpg" title="" data-lightbox="portfolio" data-title="Portfolio image 4"><images src="images/portfolio-4.jpg" alt="" class="images-responsive"></a></div>
        </div>
        <div class="col-lg-3 col-sm-4 col-xs-6">
          <div class="box"><a href="images/portfolio-5.jpg" title="" data-lightbox="portfolio" data-title="Portfolio image 5"><images src="images/portfolio-5.jpg" alt="" class="images-responsive"></a></div>
        </div>
        <div class="col-lg-3 col-sm-4 col-xs-6">
          <div class="box"><a href="images/portfolio-6.jpg" title="" data-lightbox="portfolio" data-title="Portfolio image 6"><images src="images/portfolio-6.jpg" alt="" class="images-responsive"></a></div>
        </div>
        <div class="col-lg-3 col-sm-4 col-xs-6">
          <div class="box"><a href="images/portfolio-7.jpg" title="" data-lightbox="portfolio" data-title="Portfolio image 7"><images src="images/portfolio-7.jpg" alt="" class="images-responsive"></a></div>
        </div>
        <div class="col-lg-3 col-sm-4 col-xs-6">
          <div class="box"><a href="images/portfolio-8.jpg" title="" data-lightbox="portfolio" data-title="Portfolio image 8"><images src="images/portfolio-8.jpg" alt="" class="images-responsive"></a></div>
        </div>
      </div>
    </div>
  </section>

  <script> 
    document.getElementById("home").className = "active"; 
  </script>

  
@endsection