@if(session('success'))
	<div class="modal fade" id="reserveModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="title">
						@if(isset($errors) && count($errors) > 0)
							Atenção!
						@else
							{{session('success')['messages']}}
						@endif
					</h4>
				</div>
				<div class="modal-body">
					<p>
						@if(isset(session('success')['messages_info']))
							{{session('success')['messages_info']}}
						@endif

						@if(isset($errors) && count($errors) > 0)
							@foreach($errors->all() as $error)
								<p>{{$error}}</p>
							@endforeach
						@endif
					</p>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
@endif

<div class="modal fade" id="passwordModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="title">Alterar Senha</h4>
			</div>
			<div class="modal-body">
				{!! Form::open(['route' => 'changePassword']) !!}
					<div class="row">
						<div class="form-group">
							{!! Form::label('new_password', 'Nova Senha',['class' => 'col-sm-3']) !!}

							<div class="col-sm-9">
								{!! Form::password('new_password', null, ['class' => 'form-control', 'placeholder' => 'Digite sua senha atual']) !!}
							</div>
						</div>
					</div>
					<p></p>
					<div class="row">
						<div class="form-group">
							{!! Form::label('confirm_password', 'Confirmar Senha',['class' => 'col-sm-3']) !!}

							<div class="col-sm-9">
								{!! Form::password('confirm_password', null, ['class' => 'form-control', 'placeholder' => 'Digite sua senha atual']) !!}
							</div>
						</div>
					</div>
								
			</div>
			<div class="modal-footer">
					<button class="btn btn-primary" data-dismiss="modal">Fechar</button>
					<button class="btn btn-primary" type="submit">Salvar</button>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@auth
	<div class="modal fade" id="cancelReserveModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	  <div class="modal-dialog modal-sm" role="document">
	    <div class="modal-content">
	   		<div class="modal-header">
	   			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	   			<h4 class="modal-title">Cancelar Reserva?</h4>
	   		</div>
	   		<div class="modal-body">
	   			<div class="container-fluid">
	   				{!! Form::open(['route' => 'site.user.cancel.reserve', 'method' => 'delete', 'id' => 'cancel-form']) !!}
	    				{!! Form::hidden('reserve_id', Auth::user()->getReservedBook(Auth::user()->id)) !!}
		   				<div class="row">
		   					<div class="col-sm-12">
		   						<button class="btn btn-success btn-block">Sim</button>
		   					</div>
		   				</div>
		   				<p></p>
		   				<div class="row">
		   					<div class="col-sm-12">
		   						<button class="btn btn-info btn-block" data-dismiss="modal">Não</button>
		   					</div>
		   				</div>
		   			{!! Form::close() !!}
	   			</div>
	   		</div>
	    </div>
	  </div>
	</div>
@endauth

@auth
	<div class="modal fade" id="ratingModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Sua avaliação sobre esse livro</h4>
				</div>
				<div class="modal-body">
					<div class="container-fluid">
						<div class="estrelas" id="div_estrelas_dois">
							{!! Form::open(['route' => 'book.rating', 'id' => 'form_rating']) !!}
								{!! Form::hidden('book_id') !!}
		                        <input type="radio" id="vazio" name="score" value="" checked>

		                        <label for="1"><i class="fa"></i></label>
		                        <input type="radio" name="score" value="1" id="1">

		                        <label for="2"><i class="fa"></i></label>
		                        <input type="radio" name="score" value="2" id="2">

		                        <label for="3"><i class="fa"></i></label>
		                        <input type="radio" name="score" value="3" id="3">

		                        <label for="4"><i class="fa"></i></label>
		                        <input type="radio" name="score" value="4" id="4">

		                        <label for="5"><i class="fa"></i></label>
		                        <input type="radio" name="score" value="5" id="5">
                      	</div>
					</div>
				</div>
				<div class="modal-footer">
						<button class="btn btn-primary" id="confirm">Avaliar</button>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$(document).ready(function(){
			$('a[data-confirm]').click(function(ev){
				var href = $(this).attr('href');
				var form_rating = $('#form_rating');
				href = href.replace('#', '');

				$("[name=book_id]").val(href);
				$('#confirm').on('click', function(){
					form_rating.submit();
				});
			});
		});
	</script>
@endauth