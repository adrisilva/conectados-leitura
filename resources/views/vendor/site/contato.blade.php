@extends('site.layouts.app')

@section('content')

    <section class="background-gray-lightest">
        <div class="container">
            <div class="breadcrumbs">
                <ul class="breadcrumb">
                    <li><a href="{{route('site.index')}}">Home</a></li>
                    <li>Contato</li>
                </ul>
            </div>
            <h1>Contato</h1>
            <p class="lead">Tem alguma dúvida no uso do site? Alguma sugestão ou reclamação? Contate-nos através do nosso e-mail. Caso prefira, ligue para a escola ou fale diretamente com as bibliotecárias.</p>
        </div>
    </section>

    <section>  
        <div id="contact" class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="box-simple">
                        <div class="icon"><i class="pe-7s-map-2"></i></div>
                        <div class="content">
                            <h4>Endereço</h4>
                            <p>Avenida E, 471<br>José Walter<br>Fortaleza - CE<br><strong>Brasil</strong></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box-simple">
                        <div class="icon"><i class="pe-7s-phone"></i></div>
                        <div class="content">
                            <h4>Telefone</h4>
                            <p class="text-muted">Está com dúvidas? Ligue para a escola.</p>
                            <p><strong>(85) 3101-2984</strong></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box-simple">
                        <div class="icon"><i class="pe-7s-mail-open"></i></div>
                        <div class="content">
                            <h4>Suporte Online</h4>
                            <p class="text-muted">Sinta-se à vontade para nos escrever um email.</p>
                            <ul>
                                <li><strong><a href="mailto:">scriptutorias@gmail.com</a></strong></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3980.8866739843775!2d-38.560185485148054!3d-3.834492397210637!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7c751e35869dd01%3A0xe6160def30227899!2sEEEP+Professor+On%C3%A9lio+Porto!5e0!3m2!1spt-BR!2sbr!4v1508859749552" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

    <script> 
        document.getElementById("contato").className = "active"; 
    </script>   

@endsection
