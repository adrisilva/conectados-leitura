@extends('site.layouts.app')

@section('content')

    <section class="background-gray-lightest">
        <div class="container">
            <div class="breadcrumbs">
                <ul class="breadcrumb">
                <li><a href="{{route('site.index')}}">Home</a></li>
                <li>Sobre o projeto</li>
                </ul>
            </div>
            <h1>Sobre o projeto</h1>
            <p class="lead">Você é um resumo dos livros que você lê, os filmes que assiste, as musicas que ouve, as pessoas com quem passa mais tempo e as conversas com as quais se envolve. Escolha sabiamente como você alimenta sua mente.</p>
        </div>
    </section>

    <section class="blog-post">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                <div class="post-content margin-bottom--big">

                    <strong class="topico">Como surgiu o projeto ADM?</strong><br>
                    <blockquote>
                    <p>
                        Surgiu de uma simples conversa nos corredores da escola, de um pequeno grupo de alunos com o professor de artes Wellington Saraiva no primeiro semestre de 2017, o qual disse que seria uma ótima ideia que a nossa turma criasse um sistema de livros para a biblioteca da escola. Apartir dessa sugestão, passamos a formular a melhor maneira de fazer algo do tipo além de chamar mais pessoas para participar da proposta.
                    </p>
                    </blockquote><br><br>

                    <strong class="topico">De onde veio a inspiração para construção do ADM?</strong>
                    <blockquote>
                    <p>
                        Principalmente da observação da escola como todo, de ver que a esmagadora maioria dos alunos não sabiam da amplitude de conteúdos, gostos e estilos diferentes presentes nos livros da nossa biblioteca. Muitas vezes se tornava até difícil saber se tinha um livro específico ou se tinha um livro com determinado conteúdo, seja da base técnica ou regular, além do que até alguns professores não sabiam da existência de vários livros que podem ser bastante úteis nas aulas e em pesquisas de alunos, com tudo isso também veio a vontade da turma de deixar um legado para escola, algo que mesmo após o nosso termino no ensino médio ficasse para as próximas gerações de alunos, sempre apoaindo-lhes de forma positiva e somando no aprendizado.
                    </p>
                    </blockquote><br><br>

                    <strong class="topico">Porque Alimento da mente?</strong>
                    <blockquote>
                    <p>
                        O nome foi criado por Matheus Lemos, a pessoa que esteve a frente do projeto, e decidiu este nome por representar bem o que os livros devem ser para um estudante, sempre o alimento intelectual, algo resposável por trazer e levar muito conhecimento, o verdadeiro Alimento da mente!
                    </p>
                    </blockquote><br><br>

                    <strong class="topico">Programação do ADM</strong>
                    <blockquote>
                    <p>
                        O desenvolvimento do sistema bibliotecário hoje intitulado como Alimento da Mente foi liderado por Adri Silva, que esteve presente dês do primeiro esboço do site e que conhece como ninguém o que se passa por trás nos scripts a cada clic do usuário.
                    </p>
                    </blockquote><br><br>

                    <strong class="topico">Catalogação dos livros</strong>
                    <blockquote>
                    <p>
                        O processo de catalogação dos livros foi a etapa mais demorada na construção do projeto por causa da biblioteca da escola ter MUITOS livros, então para tal tivemos uma equipe vasta de alunos trabalhando arduamente na pesquisa de imagens e cadastro dos livros no sistema, mesmo em meio a rotina escolar. A equipe de catalogação de livros para o ADM foi constituida por: Matheus Lemos, Adri Silva, Rogério Filho, Jailton Vitor, Vitória Elen, Maria da Graça, Emanoel Camurça, Williann Linhares e Samir Torres, Ufa!
                    </p>
                    </blockquote>

                    <p><img src="images/blog1.jpg" alt="Example blog post alt" class="img-responsive"></p>
                </div>
                </div>
            </div>

            <div class="row no-space padding-horizontal">
                <div class="col-sm-4 col-xs-6">
                <div class="box"><a href="images/glr-3.jpeg" title="" data-lightbox="portfolio" data-title="Turma Informática 2016-2018 completa em 2017"><img src="images/glr-3.jpeg" alt="" class="img-responsive"></a></div>
                </div>
                <div class="col-sm-4 col-xs-6">
                <div class="box"><a href="images/portfolio-4.jpg" title="" data-lightbox="portfolio" data-title="Portfolio image 4"><img src="images/portfolio-4.jpg" alt="" class="img-responsive"></a></div>
                </div>
                <div class="col-sm-4 col-xs-6">
                <div class="box"><a href="images/portfolio-5.jpg" title="" data-lightbox="portfolio" data-title="Portfolio image 5"><img src="images/portfolio-5.jpg" alt="" class="img-responsive"></a></div>
                </div>
                <div class="col-sm-4 col-xs-6">
                <div class="box"><a href="images/portfolio-6.jpg" title="" data-lightbox="portfolio" data-title="Portfolio image 6"><img src="images/portfolio-6.jpg" alt="" class="img-responsive"></a></div>
                </div>
                <div class="col-sm-4 col-xs-6">
                <div class="box"><a href="images/portfolio-7.jpg" title="" data-lightbox="portfolio" data-title="Portfolio image 7"><img src="images/portfolio-7.jpg" alt="" class="img-responsive"></a></div>
                </div>
                <div class="col-sm-4 col-xs-6">
                <div class="box"><a href="images/portfolio-8.jpg" title="" data-lightbox="portfolio" data-title="Portfolio image 8"><img src="images/portfolio-8.jpg" alt="" class="img-responsive"></a></div>
                </div>
            </div>
        </div>
    </section>

    <script>document.getElementById("sobre").className = "active"; </script> 
@endsection