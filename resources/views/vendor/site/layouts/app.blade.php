<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{$title or 'Conectados pela Leitura'}}</title>

    {{-- <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script> --}}
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-6797420998078967",
            enable_page_level_ads: true
        });
    </script>
    <link rel="stylesheet" href={{asset('css/bootstrap.min.css')}}>
    <link rel="stylesheet" href={{asset('css/spinning.css')}}>
    <!-- Font Awesome & Pixeden Icon Stroke icon font-->
    <link rel="stylesheet" href={{asset('css/font-awesome.min.css')}}>
    <link rel="stylesheet" href={{asset('css/pe-icon-7-stroke.css')}}>
    <!-- Google fonts - Roboto Condensed & Roboto-->
    <link rel="stylesheet" href={{asset('https://fonts.googleapis.com/css?family=Roboto+Condensed:700|Roboto:300,400')}}>
    <!-- lightbox-->
    {{-- <link rel="stylesheet" href={{url('css/lightbox.min.css')}}> --}}
    <!-- theme stylesheet-->
    <link rel="stylesheet" href={{asset('css/style.default.css')}} id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href={{asset('css/custom.css')}}>
    <!-- Favicon-->
    <link rel="shortcut icon" href={{asset("images/favicon.ico")}}>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.js" crossorigin="anonymous"></script>
    <script type="text/javascript">
        {{-- var name = {{Auth::user()->name}}; --}}
        //var firstname = name.split(" ");
        //$("#user_name").text(firstname[0]); 
    </script>
    <style type="text/css">
        .estrelas input[type=radio]{
            display: none;
            }.estrelas label i.fa:before{
                content: '\f005';
                color: #FC0;
                }.estrelas  input[type=radio]:checked  ~ label i.fa:before{
                    color: #CCC;
                }
    </style>
    @stack('load-class-home')
</head>
<body id="body">
    <header class="header">
        <div role="navigation" class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header"><a href="{{route('site.index')}}" class="navbar-brand">Conectados pela Leitura</a>
                <div class="navbar-buttons">
                    <button type="button" data-toggle="collapse" data-target=".navbar-collapse" class="navbar-toggle navbar-btn">Menu<i class="fa fa-align-justify"></i></button>
                </div>
            </div>
            <div id="navigation" class="collapse navbar-collapse navbar-right">
                <ul class="nav navbar-nav">
                    <li id="home"><a href="{{route('site.index')}}">Home</a></li>
                    <li id="sobre"><a href="{{route('site.sobre')}}">Sobre o projeto</a></li>
                    <li class="dropdown" id="categoria"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Categorias <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href={{url("livros/Todos")}}>Todos</a></li>
                            <li><a href={{url("livros/Artes")}}>Artes</a></li>
                            <li><a href={{url("livros/Espanhol")}}>Espanhol</a></li>
                            <li><a href={{url("livros/EducaçãoFísica")}}>Educação Física</a></li>
                            <li><a href={{url("livros/História")}}>História</a></li>
                            <li><a href={{url("livros/Inglês")}}>Inglês</a></li>
                            <li><a href={{url("livros/Literatura")}}>Literatura</a></li>
                            <li><a href={{url("livros/Música")}}>Música</a></li>
                            <li><a href={{url("livros/Enfermagem")}}>Técnico em Enfermagem</a></li>
                            <li><a href={{url("livros/Finanças")}}>Técnico em Finanças</a></li>
                            <li><a href={{url("livros/Informática")}}>Técnico em Informática</a></li>
                            <li><a href={{url("livros/Moda")}}>Técnico em Produção de Moda</a></li>
                        </ul>
                    </li>
			    <li id="contato"><a href="https://www.alimentodamente.com.br/contact.php">Contato</a></li>
                    @auth
                        <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle" id="user_name">{{Auth::user()->name}} <i class="far fa-user-circle"></i><b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href={{route('historic')}}>&nbsp;<i class="fas fa-book"></i> Histórico</a>
                                </li>
                                <li>
                                    <a href="#" data-toggle="modal" data-target="#passwordModal">&nbsp;<i class="fas fa-key"></i> Mudar Senha</a>
                                </li>
                                <li>
                                    @if(Auth::user()->getReservedBook(Auth::user()->id) != null)
                                        {{-- <a href="#" onclick="event.preventDefault(); document.getElementById('cancel-form').submit();">&nbsp;<i class="fas fa-ban"></i> Cancelar Reserva Atual</a> --}}
                                        <a href="#" data-toggle="modal" data-target="#cancelReserveModal">&nbsp;<i class="fas fa-ban"></i> Cancelar Reserva Atual</a>
                                        
                                    @endif
                                </li>
                                <li>
                                    <a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <i class="fa fa-fw fa-power-off"></i>Sair
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>   
                            </ul>
                        </li>
                    @else
                        </ul><a href="#" data-toggle="modal" data-target="#login-modal" class="btn navbar-btn btn-ghost" id="btnLogin"> <i class="fas fa-sign-in-alt"></i> Login</a>
                    @endauth
                </div>
            </div>
        </div>
    </header>

    <!-- JANELA MODAL DE LOGIN-->
    <div id="login-modal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true" class="modal fade">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close">×</button>
            <h4 id="Login" class="modal-title">Login</h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('login') }}" method="post">
                @csrf

                <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                    <input type="email" name="email" class="form-control" value="{{ old('email') }}"
                            placeholder="E-Mail">
                    {{-- <span class="glyphicon glyphicon-envelope form-control-feedback"></span> --}}
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                    <input type="password" name="password" class="form-control"
                            placeholder="Senha">
                    {{-- <span class="glyphicon glyphicon-floppy-disk form-control-feedback"></span> --}}
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

                <p class="text-center">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Entrar</button>
                </p>
            </form>
            <p class="text-center text-muted">Não é cadastrado?</p>
            <p class="text-center text-muted"><font color="#f06190"><strong>Procure a biblioteca da escola!</strong></font></a> É facil, rápido, gratuito e sempre será!</p>
      
          </div>
        </div>
      </div>
    </div>
    
    @yield('content')

    <footer class="footer">
        <div class="footer__copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <p>&copy; 2018 - Todos os direitos reservados | <a href="{{route('site.contato')}}">Desenvolvido pela equipe ADM</a></p>
                    </div>
                    <div class="col-md-6">
                        <p class="credit">Sistema Bibliotecário da EEEP. Professor Onélio Porto</a></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    
    @include('site.includes.modal')
    
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> --}}
    <script src={{asset("js/bootstrap.min.js")}}></script>
    <script src={{asset("js/jquery.cookie.js")}}> </script>
    {{-- <script src={{url("js/lightbox.min.js")}}></script> --}}
    <script src={{asset("js/front.js")}}></script>
    
    @if($errors->has('password') || $errors->has('email'))
        <script type="text/javascript">
            $('#login-modal').modal('show');
        </script>
    @endif
    
    @if(session('success'))
        <script type="text/javascript">
            $('#reserveModal').modal('show');
        </script>
    @endif
</body>
</html>
