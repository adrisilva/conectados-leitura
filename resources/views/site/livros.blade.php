@extends('site.layouts.app')

@section('content')
    <div id="loading-screen" style="display:none">
        <img src={{asset("images/spinning-circles.svg")}}>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <strong>Categoria: {{$categoria or $data['category']}}</strong>
                <h4>Faça a busca de um livro</h4>

                <div>
                    {!! Form::model($data, ['url' => ['livros-search']]) !!}
                   
                    {!! Form::label('search', 'Buscar', ['class' => 'col-sm-1']) !!}

                    <div class="col-sm-2">
                        {!! Form::text('search', null, ['class' => 'form-control input-sm', 'placeholder' => 'Busca']) !!}
                    </div>

                    {!! Form::label('filter', 'Filtro', ['class' => 'col-sm-1']) !!}

                    <div class="col-sm-2">
                        {!! Form::select('filter',['book_title' => 'Título do Livro', 'author_name' => 'Nome do Autor'] ,null, ['class' => 'form-control input-sm']) !!}
                    </div>

                    {!! Form::label('category', 'Categoria', ['class' => 'col-sm-1']) !!}

                    <div class="col-sm-3">
                       {!! Form::select('category', $categories , null, ['class' => 'form-control']); !!}
                    </div>

                    <div class="col-sm-2">
                        <button class="btn btn-primary"><i class="fa fa-search"></i> Buscar</button>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>

        <p></p>
        
        @forelse($livros as $livro)
            <div class="col-sm-3" id="thumbnail" style="height: 500px;">
                <div class="thumbnail">
                    <img src="{{ asset('covers/'.$livro->cover) }}" width="80%" height="25%">
                    <div class="caption">
                        <h4>{{$livro->title}}</h4>
                        <p>
                            <a href='{{"#".$livro->id}}' class="btn btn-primary" data-confirm>Ver Mais</a>
                        </p>
                    </div>
                </div>
            </div>
        @empty
            <h3>Nenhum Livro Encontrado</h3>
        @endforelse

        <div class="row">
            <div class="col-sm-12">
                @if($data != null)
                    {{$livros->appends($data)->links()}}
                @else
                    {{$livros->links()}}
                @endif
            </div>
        </div>

        <div class="modal fade" id="bookModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="title">Título</h4>
                    </div>
                    <div class="modal-body">
                        <p><b>Autor: </b> <span id="author"></span></p>
                        <p><b>Quantidade Disponível: </b> <span id="amount"></span></p>
                        <p><b>Descrição: </b> <span id="description"></span></p>
                    </div>
                    <div class="modal-footer">
                       @auth
                            <div class="row">
                                <div class="col-sm-3">
                                    {!! Form::open(['url' => ['/reservar/livro']]) !!}
                                        {!! Form::hidden('book_id') !!}
                                        <button class="btn btn-primary">Reservar</button>
                                    {!! Form::close() !!}
                                </div>
                                <div class="col-sm-3">
                                    @if(Auth::user()->type == "Professor")
                                        {!! Form::open(['url' => ['/recomendar/livro']]) !!}
                                            {!! Form::hidden('book_id') !!}
                                            <button class="btn btn-primary">Recomendar</button>
                                        {!! Form::close() !!}
                                    @endif
                                </div>
                            </div>
                       @endauth
                    </div>
                </div>
            </div>
        </div>

    </div>

    <script type="text/javascript">
        document.getElementById("categoria").className = "active"; 

        $(document).ready(function(){
            var screen = $('#loading-screen');
            configureLoadingScreen(screen);

            $('a[data-confirm]').click(function(ev){
                var book_id = $(this).attr('href').replace('#', '');
                var url = "{{ url('/livro/show/_id_') }}".replace('_id_', book_id);
                $.ajaxSetup({
                    url: url,
                    success: function(result){
                        $("[name=book_id]").val(book_id);
                        $("#title").text(result.title);
                        $("#author").text(result.name);
                        $("#amount").text(result.amount);
                        $("#description").text(result.description);
                        $('#bookModal').modal('show');
                    }
                });
                $.ajax();
            });
        });

        function configureLoadingScreen(screen){
            $(document)
                .ajaxStart(function () {
                    screen.fadeIn();
                })
                .ajaxStop(function () {
                    screen.fadeOut();
                });
        }
    </script>

@endsection