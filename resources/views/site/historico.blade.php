@extends('site.layouts.app')

@section('content')
	<div class="container">
		<h3>Seu Histórico</h3>

		<table class="table">
			<thead>
				<tr>
					<th>Livro</th>
					<th>Data da Reserva</th>
					<th>Data da Devolução</th>
					<th>Status</th>
					<th>Avaliação</th>
				</tr>
			</thead>
			<tbody>
				@forelse($historic as $row)
					<tr>
						<td>{{$row->title}}</td>
						<td>{{$row->reservation_date->format('d/m/Y')}}</td>
						<td>
							@if($row->return_date != null)
								{{$row->return_date->format('d/m/Y')}}
							@else
								-
							@endif
						</td>
						<td>
							{{$row->status}}
						</td>
						<td>
							@if($row->status == "Entregue")
								<a href={{"#".$row->book_id}} class="btn btn-primary btn-sm" data-toggle="modal" data-target="#ratingModal" data-confirm="avaliar">Avaliar</a>
							@else
								<a class="btn btn-primary btn-sm"" disabled>Avaliar</a>
							@endif
						</td>
					</tr>
				@empty
					<tr>
						<th>Nada</th>
					</tr>
				@endforelse
			</tbody>
		</table>
	</div>
@endsection