@extends('site.layouts.app')

@section('content')
    <div class="container">
        <h3>Suas Recomendações</h3>

        <table class="table">
            <thead>
            <tr>
                <th>Livro</th>
                <th>Data da Recomendação</th>
                <th>Remover Recomendação</th>
            </tr>
            </thead>
            <tbody>
            @forelse($recomendations as $row)
                <tr>
                    <td>{{$row->book->title}}</td>
                    <td>{{\Carbon\Carbon::parse($row->recommendation_date)->format('d/m/Y')}}</td>
                    <td>
                        <a href="{!! url("remover/{$row->id}/recomendacao")  !!}" class="btn btn-primary">Remover</a>
                    </td>
                </tr>
            @empty
                <tr>
                    <th>Nenhuma Recomendação</th>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
@endsection