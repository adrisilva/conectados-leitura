@extends('site.layouts.app')

@prepend('load-class-home')
    {{--<script>--}}
    {{--document.getElementById("body").className = "home"; --}}
    {{--document.getElementById("btnLogin").className = "btn navbar-btn btn-white";--}}
    {{--</script>--}}
@endprepend

@section('content')

    <div id="loading-screen" style="display:none">
        <img src={{asset("images/spinning-circles.svg")}}>
    </div>

    <div id="carousel-home" data-ride="carousel" class="carousel slide carousel-fullscreen carousel-fade">
        <!-- Indicators-->
        <ol class="carousel-indicators">
            <li data-target="#carousel-home" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-home" data-slide-to="1"></li>
            <li data-target="#carousel-home" data-slide-to="2"></li>
        </ol>
        <!-- Wrapper for slides-->
        <div role="listbox" class="carousel-inner">
            <div style="background-image: url('images/carousel3.jpg');" class="item active">
                <div class="overlay"></div>
                <div class="carousel-caption">
                    <h1 class="super-heading">Alimento da Mente</h1>
                    <p class="super-paragraph">Escola Estadual de Educação Profissional Professor Onélio Porto</a></p>
                </div>
            </div>
            <div style="background-image: url('images/carousel2.jpg');" class="item">
                <div class="overlay"></div>
                <div class="carousel-caption">
                    <h1 class="super-heading">Web sistema bibliotecário</h1>
                    <p class="super-paragraph">Desenvolvido pela turma de Informática 2016-2018</p>
                </div>
            </div>
            <div style="background-image: url('images/carousel1.jpg');" class="item">
                <div class="overlay"></div>
                <div class="carousel-caption">
                    <h1 class="super-heading">Leia um livro hoje</h1>
                    <p class="super-paragraph">"O homem que não lê bons livros não tem nenhuma vantagem sobre o homem que não sabe ler."</p>
                </div>
            </div>
        </div>
    </div>
    <section class="background-gray-lightest">
        <div class="container">
            <h1> O que é o projeto Alimento da Mente?</h1>
            <p class="lead">&ensp;O Alimento da Mente(ADM) é o web sistema bibliotecário para consulta de livros técnicos e de base regular da EEEP Professor Onélio Porto, feito para auxiliar na vida acadêmica de todos os alunos e professores da escola.<br>&ensp;O sistema conta com um avançado sistema de pesquisa com todos os livros disponíveis na biblioteca da escola, desenvolvido pela turma do curso de Informática 2016-2018.</p>
            <p> <a href="text.php" class="btn btn-ghost">Saiba mais   </a></p>
        </div>
    </section>
    <!-- portfolio-->
    <section id="portfolio" class="background-gray-lightest">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1>Livros Recomendados pelos Professores</h1>
                </div>
            </div>
        </div>


        <div class="container slider multiple-items">

            @forelse($recomendacoes as $livro)
                <div>
                    <div>
                        <div>
                            <img src="{{ asset('covers/'.$livro->book->cover) }}">
                            <div class="caption">
                                <h4>{{$livro->title}}</h4>
                                <p>
                                    <a href='{{"#".$livro->book->id}}' class="btn btn-primary" data-confirm>Ver Mais</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
                <div>
                    <h4>Nenhum livro foi recomendado até agora</h4>
                </div>
            @endforelse
        </div>

        <div class="container">
            <button type="button" class="btn btn-primary slick-prev" id="prev"><i class="fa fa-arrow-left"></i></button>
            <button type="button" class="btn btn-primary slick-prev" id="next"><i class="fa fa-arrow-right"></i></button>
        </div>


    </section>

    <div class="modal fade" id="bookModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="title">Título</h4>
                </div>
                <div class="modal-body">
                    <p><b>Autor: </b> <span id="author"></span></p>
                    <p><b>Quantidade Disponível: </b> <span id="amount"></span></p>
                    <p><b>Descrição: </b> <span id="description"></span></p>
                </div>
                <div class="modal-footer">
                    @auth
                        <div class="row">
                            <div class="col-sm-3">
                                {!! Form::open(['url' => ['/reservar/livro']]) !!}
                                {!! Form::hidden('book_id') !!}
                                <button class="btn btn-primary">Reservar</button>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    @endauth
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

    <script>
        $('.multiple-items').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 3000,
            nextArrow: $('#next'),
            prevArrow: $('#prev'),
        });

        $(document).ready(function(){
            var screen = $('#loading-screen');
            configureLoadingScreen(screen);

            $('a[data-confirm]').click(function(ev){
                var book_id = $(this).attr('href').replace('#', '');
                var url = "{{ url('/livro/show/_id_') }}".replace('_id_', book_id);
                $.ajaxSetup({
                    url: url,
                    success: function(result){
                        $("[name=book_id]").val(book_id);
                        $("#title").text(result.title);
                        $("#author").text(result.name);
                        $("#amount").text(result.amount);
                        $("#description").text(result.description);
                        $('#bookModal').modal('show');
                    }
                });
                $.ajax();
            });
        });

        function configureLoadingScreen(screen){
            $(document)
                .ajaxStart(function () {
                    screen.fadeIn();
                })
                .ajaxStop(function () {
                    screen.fadeOut();
                });
        }
    </script>



@endsection