$('#btnSearch').click(function(){
	var searchType  = $('#searchType').val();
	var search 		= $('#search').val();
	var dataForm  	= 'search='+search;
	var url; 	

	if(searchType == 0)
		url = "{{ route('aluno.search-lite', '_data_') }}".replace('_data_', dataForm);
	else
		url = "{{ route('professor.search-lite', '_data_') }}".replace('_data_', dataForm);

	$.get(url, function(data){
		$("#tbody").empty();
		var count = Object.keys(data).length;
		if(count == 0){
			$("#tbody").append('<tr><th colspan=5>Nenhum Usuário Encontrado<th></tr>');
		}else{
			$.each(data, function(key, value){
				var nome  = value.name;
				var curso = (value.course == null) ? "-" : value.course;
				var serie = (value.series == "º Ano") ? "-" :value.series;
				$("#tbody").append(
					'<tr><td>'+value.registration+'</td>'+
					'<td>'+nome+'</td>'+
					'<td>'+curso+'</td>'+
					'<td>'+serie+'</td>'+
					'<td><button class="btn-link selecionar">Selecionar</button></td></tr>'
				);
			});
		}
		setDataAluno();
	});
});	

function setDataAluno()
{
	$('.selecionar').click(function(){
		var matricula = $(this).parents('tr').find('td').eq(0).text();
		var name = $(this).parents('tr').find('td').eq(1).text();
		var curso = $(this).parents('tr').find('td').eq(2).text();
		$('#name').val(name);
		$('#course').val(curso);
		$('#registration').val(matricula);
		$('#user_registration').val(matricula);
		$('#btnConcluir').prop("disabled", false);
	});
}