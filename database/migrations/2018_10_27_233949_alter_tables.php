<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table){
            $table->unsignedInteger('school_id')->nullable();
            $table->foreign('school_id')->references('id')->on('schools')->onDelete('cascade');
            $table->softDeletes();
        });

        Schema::table('admins', function(Blueprint $table){
            $table->unsignedInteger('school_id')->nullable();
            $table->foreign('school_id')->references('id')->on('schools')->onDelete('cascade');
            $table->softDeletes();
        });

        Schema::table('books', function(Blueprint $table){
            $table->unsignedInteger('school_id')->nullable()->default(1);
            $table->foreign('school_id')->references('id')->on('schools')->onDelete('cascade');
            $table->softDeletes();
        });

        Schema::table('reserves', function(Blueprint $table){
            $table->unsignedInteger('school_id')->nullable();
            $table->foreign('school_id')->references('id')->on('schools')->onDelete('cascade');
            $table->softDeletes();
        });

        Schema::table('ratings', function(Blueprint $table){
            $table->unsignedInteger('school_id')->nullable();
            $table->foreign('school_id')->references('id')->on('schools')->onDelete('cascade');
            $table->softDeletes();
        });

        Schema::table('recommendations', function(Blueprint $table){
            $table->unsignedInteger('school_id')->nullable();
            $table->foreign('school_id')->references('id')->on('schools')->onDelete('cascade');
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table){
            $table->dropForeign('school_id');
            $table->dropColumn('school_id');
            $table->dropSoftDeletes();
        });

        Schema::table('admins', function(Blueprint $table){
            $table->dropForeign('school_id');
            $table->dropColumn('school_id');
            $table->dropSoftDeletes();
        });

        Schema::table('books', function(Blueprint $table){
            $table->dropForeign('school_id');
            $table->dropColumn('school_id');
            $table->dropSoftDeletes();
        });

        Schema::table('reserves', function(Blueprint $table){
            $table->dropForeign('school_id');
            $table->dropColumn('school_id');
            $table->dropColumn('deleted_at');
        });
        
        Schema::table('ratings', function(Blueprint $table){
            $table->dropForeign('school_id');
            $table->dropColumn('school_id');
            $table->dropSoftDeletes();
        });
       
        Schema::table('recommendations', function(Blueprint $table){
            $table->dropForeign('school_id');
            $table->dropColumn('school_id');
            $table->dropSoftDeletes();
        });
    }
}
