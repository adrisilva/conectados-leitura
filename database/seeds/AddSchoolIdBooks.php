<?php

use Illuminate\Database\Seeder;
use \App\Entities\Book;

class AddSchoolIdBooks extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $books = Book::all();
        $school = \App\Entities\School::where('slug', 'onelio')->first();
        $school_id = $school->id;

        foreach ($books as $book){
            $book->school_id = $school_id;
            $book->save();
        }
    }
}
