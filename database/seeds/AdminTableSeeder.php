<?php

use App\Entities\Admin;
use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::create([
        	'name' 		=> 'Admin',
        	'email'		=> 'admin@email.com',
        	'password'	=> bcrypt('admin'),
        	'phone'		=> '40028922'
        ]);
    }
}
